(($) => {
    document.addEventListener('DOMContentLoaded', () => {
        const $accordionHeaders = $('.single-product-accordion .accordion-section .accordion-header');

        $accordionHeaders.on('click', function() {
            const $content = $(this).next('.accordion-content');
            const $section = $(this).parent('.accordion-section');

            // Toggle the active class for the clicked section
            $section.toggleClass('active');
            
            $content.slideToggle();

            // If you want only one section open at a time:
            $('.accordion-content').not($content).slideUp();
            $('.accordion-section').not($section).removeClass('active'); // Remove the active class from all other sections
        });
    });
})(jQuery);
