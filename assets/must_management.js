document.addEventListener('DOMContentLoaded', function () {
  // 국가 필터 폼을 찾습니다.
  var countryFilterForm = document.getElementById('country-filter-form');

  // 벌크 액션 폼을 찾습니다.
  var bulkActionForm = document.getElementById('must-user-management-bulk-form');

  // 벌크 액션 폼의 div 컨테이너를 찾습니다.
  var bulkActionsContainer = bulkActionForm.querySelector('.bulkactions');

  // 국가 필터 폼을 벌크 액션 div 컨테이너 옆으로 이동시킵니다.
  bulkActionsContainer.parentNode.insertBefore(countryFilterForm, bulkActionsContainer.nextSibling);

  // 필요한 경우 추가적인 CSS 스타일링을 적용합니다.
  countryFilterForm.style.display = 'inline-block';
  bulkActionsContainer.style.display = 'inline-block';
});

