<?php
define('APMMUST_BYPASS_CACHE', false);

/**
 * Shoptimizer child theme functions
 *
 * @package shoptimizer
 */
// 커스텀 CSS, JS파일 로드
function apmmust_enqueue_styles() {
    // Enqueue parent style
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    
    // Enqueue custom compiled CSS
    wp_enqueue_style('custom-style', get_stylesheet_directory_uri() . '/assets/theme-custom.css', array(), '1.1.1');

    // Menu Style
    wp_enqueue_style('apmmust-menu-style', get_stylesheet_directory_uri() . '/assets/menu-style.css', array(), '1.0.3');

    // Enqueue custom JS
    wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/assets/tab-custom.js', array('jquery'), '', true);
}
add_action('wp_enqueue_scripts', 'apmmust_enqueue_styles');

// GA4 관련
require_once get_stylesheet_directory() . '/includes/ga4/ga4_connect.php';
require_once get_stylesheet_directory() . '/includes/ga4/ga4_track.php';

// Shoptimizer 테마 커스텀 관련
require_once get_stylesheet_directory() . '/includes/theme/shoptimizer_custom.php';
require_once get_stylesheet_directory() . '/includes/theme/product_tab_custom.php';
require_once get_stylesheet_directory() . '/includes/theme/marquee_widget.php';
require_once get_stylesheet_directory() . '/includes/theme/desktop_navigation.php';

// 기본 워드프레스/우커머스 커스텀 관련
require_once get_stylesheet_directory() . '/includes/base/wordpress_custom.php';
require_once get_stylesheet_directory() . '/includes/base/users_page_custom.php';
require_once get_stylesheet_directory() . '/includes/base/redirect_rule.php';

// 머스트 유저 관련
require_once get_stylesheet_directory() . '/includes/user/user_role.php';
require_once get_stylesheet_directory() . '/includes/user/user_register_custom.php';
require_once get_stylesheet_directory() . '/includes/user/user_profile_edit_page.php';
require_once get_stylesheet_directory() . '/includes/user/must_user_management.php';
require_once get_stylesheet_directory() . '/includes/user/must_vendor_management.php';
//user_deactivate_temporary.php는 서버크론으로 매일 12시 20분에 작동
//require_once get_stylesheet_directory() . '/includes/user/user_deactivate_temporary.php';

// 머스트 이메일 관련
require_once get_stylesheet_directory() . '/includes/emails/must_emails.php';

// 상품 커스텀 관련
require_once get_stylesheet_directory() . '/includes/product/yoon_product.php';
require_once get_stylesheet_directory() . '/includes/product/must_product.php';
require_once get_stylesheet_directory() . '/includes/product/must_discount.php';
require_once get_stylesheet_directory() . '/includes/product/must_custom_order.php';

// 브랜드관련 커스텀
require_once get_stylesheet_directory() . '/includes/brand/brand_list.php';
require_once get_stylesheet_directory() . '/includes/brand/brand_hierarchical.php';
require_once get_stylesheet_directory() . '/includes/brand/apmmust_required_brand_setting.php';


// phpinfo 보기
// opcache 활성 상태를 보기 위해 임시로 작성했습니다
// if (isset($_GET['show_phpinfo']) && $_GET['show_phpinfo']) {
//  phpinfo();
//  exit;
// }
// 
