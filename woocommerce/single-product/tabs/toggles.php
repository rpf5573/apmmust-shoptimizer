<?php
defined( 'ABSPATH' ) || exit;

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

$loop_count = 0;
if ( ! empty( $product_tabs ) ) : ?>
	<div class="single-product-accordion">
		<div class="apmmust-accordion" data-multi-open="0">
			<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
				<?php
				$loop_count++;
				$item_classes = 'accordion-section';
				?>
				<div class="<?php echo esc_attr( $item_classes ); ?>"
				     id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel"
				     aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">

					<div class="accordion-header">
						<div class="accordion-title-wrapper">
							<?php printf( '<%1$s class="accordion-title">%2$s</%1$s>', 'h4', wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ) ); ?>
						</div>
						<div class="accordion-icons">
							<span class="accordion-icon opened-icon">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"/></svg>
							</span>
											<span class="accordion-icon closed-icon">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg>
							</span>
						</div>
					</div>
					<div class="accordion-content">
						<?php
						if ( isset( $product_tab['callback'] ) ) {
							call_user_func( $product_tab['callback'], $key, $product_tab );
						}
						?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
