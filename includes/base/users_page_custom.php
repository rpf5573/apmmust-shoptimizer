<?php


// 사용자 리스트 페이지에 사용자의 국가를 보이도록 하고
// 국가별 필터/정렬 기능을 추가합니다
// 국가별 필터/정렬 기능 추가 코드 - 시작
function apmmust_get_user_country($country_code)
{
  $countries = WC()->countries->get_countries();
  if (!isset($countries[$country_code])) {
    return '';
  }
  return $countries[$country_code];
}

function apmmust_get_all_countries()
{
  global $wpdb;
  $users = $wpdb->get_col("SELECT ID FROM $wpdb->users");

  $countries = array();
  foreach ($users as $user_id) {
    $country = get_user_meta($user_id, 'billing_country', true);
    if (!empty($country) && !in_array($country, $countries)) {
      $countries[] = $country;
    }
  }
  sort($countries);
  return $countries;
}

add_filter('manage_users_columns', 'apmmust_add_country_column');
function apmmust_add_country_column($columns)
{
  $columns['billing_country'] = 'Country';
  return $columns;
}

add_action('manage_users_custom_column', 'apmmust_show_country_data', 10, 3);
function apmmust_show_country_data($value, $column_name, $user_id)
{
  if ($column_name === 'billing_country') {
    $country_code = get_user_meta($user_id, 'billing_country', true);
    return apmmust_get_user_country($country_code);
  }
  return $value;
}

add_filter('manage_users_sortable_columns', 'apmmust_sortable_billing_country_column');
function apmmust_sortable_billing_country_column($columns)
{
  return wp_parse_args(
    array(
      'billing_country' => 'billing_country'
    ),
    $columns
  );
}

add_action('pre_get_users', 'apmmust_sort_billing_country_column');
function apmmust_sort_billing_country_column($query)
{
  global $pagenow;
  if (!is_admin() || 'users.php' !== $pagenow)
    return $query;

  if (!$query->get('orderby')) {
    return $query;
  }
  $orderby = $query->get('orderby');
  if ($orderby !== 'billing_country')
    return $query;

  $query->set('meta_key', 'billing_country');
  return $query;
}

add_action('restrict_manage_users', 'apmmust_add_filter_by_country_filter');
function apmmust_add_filter_by_country_filter()
{
  $countries = apmmust_get_all_countries();
  $country_map = WC()->countries->get_countries();

  if (isset($_GET['filter_by_country'])) {
    $section = $_GET['filter_by_country'];
    $section = !empty($section[0]) ? $section[0] : $section[1];
  } else {
    $section = -1;
  }

  echo ' <select name="filter_by_country[]" style="float:none;"><option value="">' . esc_html__('Country Filter', 'apmmust') . '</option>';

  foreach ($countries as $country_code) {
    $country_name = $country_map[$country_code];
    $selected = $country_code === $section ? ' selected="selected"' : '';
    echo '<option value="' . $country_code . '"' . $selected . '>' . $country_name . '</option>';
  }

  echo '</select>';
  echo '<input type="submit" class="button" value="Filter">';

  remove_action('restrict_manage_users', 'apmmust_add_filter_by_country_filter');
}

add_filter('pre_get_users', 'apmmust_filter_users_by_filter_by_country');
function apmmust_filter_users_by_filter_by_country($query)
{
  global $pagenow;
  if (!is_admin() || 'users.php' !== $pagenow)
    return $query;

  $empty = 'yes';
  if (!empty($_GET['filter_by_country'])) {
    foreach ($_GET['filter_by_country'] as $item) {
      if (!empty($item)) {
        $empty = 'no';
      }
    }
  }

  if ($empty === 'yes')
    return $query;

  $country_code = $_GET['filter_by_country'];
  $country_code = !empty($country_code[0]) ? $country_code[0] : $country_code[1];

  $meta_query = array(
    'relation' => 'AND',
    array(
      'key' => 'billing_country',
      'value' => $country_code,
      'compare' => '=='
    ),
  );
  $query->set('meta_query', $meta_query);
  return $query;
}
// 국가별 필터/정렬 기능 추가 코드 - 끝

// 고객 별 고객가입 날짜 조회 Users>All Users
add_filter('manage_users_columns', 'add_registered_date_column');
function add_registered_date_column($columns) {
    $columns['registered_date'] = __('Registered Date', 'woocommerce');
    return $columns;
}

add_action('manage_users_custom_column', 'show_registered_date_column', 10, 3);
function show_registered_date_column($value, $column_name, $user_id) {
    if ($column_name === 'registered_date') {
        $user = get_userdata($user_id);
        $registered_date = $user->user_registered;
        $days_diff = round((time() - strtotime($registered_date)) / (60 * 60 * 24));

        if ($days_diff >= 1) {
            return date("Y-m-d", strtotime($registered_date)) . ' / ' . $days_diff . ' days ago';
        } else {
            return date("Y-m-d", strtotime($registered_date));
        }
    }
    return $value;
}

add_filter('manage_users_sortable_columns', 'sortable_registered_date_column');
function sortable_registered_date_column($columns) {
    $columns['registered_date'] = 'registered_date';
    return $columns;
}

add_action('pre_get_users', 'sort_registered_date_column');
function sort_registered_date_column($query) {
    global $pagenow;
    if (!is_admin() || 'users.php' !== $pagenow) {
        return $query;
    }

    if (!$query->get('orderby')) {
        return $query;
    }

    $orderby = $query->get('orderby');
    if ($orderby !== 'registered_date') {
        return $query;
    }

    $query->set('orderby', 'registered');
    return $query;
}


// 사용자 목록에 'Company' 컬럼 추가
function add_company_column($columns) {
    $columns['company'] = 'Company'; // 'Company'라는 제목의 새 컬럼 추가
    return $columns;
}
add_filter('manage_users_columns', 'add_company_column');

// 'Company' 컬럼에 데이터 출력
function show_company_data_custom_column($value, $column_name, $user_id) {
    if ('company' == $column_name) {
        // 우커머스의 청구 정보에서 'billing_company' 메타 데이터를 가져와 출력
        return get_user_meta($user_id, 'billing_company', true);
    }
    return $value;
}
add_action('manage_users_custom_column', 'show_company_data_custom_column', 10, 3);
