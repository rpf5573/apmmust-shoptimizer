<?php
// 시작 부분에서 HTTP 헤더가 이미 전송되었는지 확인합니다.
// 만약 헤더가 전송되었다면, 어느 파일과 줄에서 발생했는지 알려주고 스크립트를 종료합니다.
if (headers_sent($file, $line)) {
  die("Headers sent in $file on line $line");
}

ob_start();  // 출력 버퍼링을 시작합니다.

// 크롤러인지 확인하는 함수입니다.
// 주요 검색 엔진의 크롤러를 식별하기 위한 목록을 포함하고 있습니다.
function is_crawler()
{
  $botlist = array(
    "Googlebot",
    "Bingbot",
    "Yahoo! Slurp",
    "DuckDuckBot",
    "Baiduspider",
    "YandexBot",
    "Sogou Spider",
    "Exabot",
    "SeznamBot",
    "Naverbot",
    "Daum",
    "Facebook External Hit",
    "Twitterbot",
    "Pinterestbot",
    "LinkedInBot",
    // 필요한 경우 여기에 다른 봇 사용자 에이전트를 추가할 수 있습니다.
  );

  // HTTP_USER_AGENT가 설정되었는지 확인합니다.
  if (!isset($_SERVER['HTTP_USER_AGENT'])) {
    return false;
  }

  // 사용자 에이전트 문자열에 크롤러가 포함되어 있는지 확인합니다.
  foreach ($botlist as $bot) {
    if (strpos($_SERVER['HTTP_USER_AGENT'], $bot) !== false) {
      return true; // 크롤러가 발견되면 true를 반환합니다.
    }
  }

  return false; // 크롤러가 발견되지 않으면 false를 반환합니다.
}

// 현재 페이지가 특정 상품 브랜드의 페이지인지 확인하는 함수입니다.
function is_product_brand()
{
  return is_tax('product_brand');
}

// 사용자 그룹을 확인하고 필요한 경우 리디렉션하는 함수입니다.
function unified_redirect_logic()
{
  global $wpdb; // 전역 $wpdb 객체를 사용하여 데이터베이스 쿼리를 수행합니다.
  $user_id = get_current_user_id(); // 현재 로그인한 사용자의 ID를 가져옵니다.

  // 'my-account' 페이지는 리디렉션에서 제외합니다.
  if (is_page('my-account')) {
    return;
  }

  // 현재 URL을 가져옵니다.
  $current_url = (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');

  // 커스텀 경로 확인을 위한 조건을 추가합니다.
  $is_custom_path = strpos($current_url, '/wishlist/') !== false ||
    strpos($current_url, '/brand-list/') !== false ||
    strpos($current_url, '/shipping-calculator/') !== false;

  // 대상 페이지가 상품 관련 페이지 또는 추가된 커스텀 경로인지 확인합니다.
  $target_pages = is_product_category() || is_product_brand() || is_product() ||
    is_cart() || is_checkout() || $is_custom_path;


  // 로그인하지 않은 사용자와 크롤러 처리
  if (!is_user_logged_in()) {
    // 크롤러가 아닌 경우에만 리디렉션
    if (!is_crawler() && $target_pages) {
      $redirect_url = home_url('/my-account/') . '?redirect_to=' . urlencode($_SERVER['REQUEST_URI']);
      wp_safe_redirect($redirect_url);
      exit;
    }
    return; // 크롤러이거나 로그인하지 않은 사용자에 대한 처리 종료
  }

  // 로그인한 사용자의 사용자 그룹을 확인합니다.
  $user_group = get_user_meta($user_id, 'user_group', true);

  // 사용자 그룹에 따라 리디렉션을 수행합니다.
  if ($target_pages) {
    switch ($user_group) {
      case 'pending':
        wp_safe_redirect(home_url('/under-review/'));
        exit;
      case 'deactivated':
        wp_safe_redirect(home_url('/deactivated/'));
        exit;
      case 'disapproval':
        wp_safe_redirect(home_url('/disapproval/'));
        exit;
      case 'must_vendor':
        wp_safe_redirect(home_url('/vendor-inaccesible/'));
        exit;
    }
  }
}
add_action('template_redirect', 'unified_redirect_logic', 10);

// 로그인 후 사용자를 원래 요청한 페이지로 리디렉션하는 함수입니다.
function after_login_redirect($redirect_to, $user = null)
{
  // 사용자 ID를 기반으로 사용자 그룹을 확인합니다.
  $user_id = get_current_user_id();
  $user_group = get_user_meta($user_id, 'user_group', true);

  // 'redirect_to' 쿼리 파라미터가 설정되어 있고 사용자 그룹이 'pending', 'disapproval', 'deactivated'가 아닌 경우
  if (isset($_GET['redirect_to']) && !in_array($user_group, ['pending', 'disapproval', 'deactivated', 'must_vendor'])) {
    // 안전한 URL 처리를 위해 esc_url_raw를 사용하여 리디렉션합니다.
    return esc_url_raw($_GET['redirect_to']);
  } elseif (in_array($user_group, ['pending', 'disapproval', 'deactivated'])) {
    // 특정 사용자 그룹에 대한 사전 정의된 리디렉션 페이지로 이동
    switch ($user_group) {
      case 'pending':
        return home_url('/under-review/');
      case 'disapproval':
        return home_url('/disapproval/');
      case 'deactivated':
        return home_url('/deactivated/');
      case 'must_vendor':
        return home_url('/vendor-inaccesible/');
    }
  }
  // 기본 리디렉션 로직
  return $redirect_to;
}
add_filter('woocommerce_login_redirect', 'after_login_redirect', 10, 2);


// 특정 사용자 그룹의 사용자에게 가격을 숨기는 함수입니다.
function custom_hide_price_logic($price)
{
  $user_id = get_current_user_id(); // 현재 로그인한 사용자의 ID를 가져옵니다.

  // 로그인하지 않은 사용자와 크롤러에게 가격 정보를 숨깁니다.
  if (!is_user_logged_in() && !is_crawler()) {
    return ''; // 가격 정보를 숨깁니다.
  }

  if ($user_id) {
    $user_group = get_user_meta($user_id, 'user_group', true); // 사용자의 그룹을 가져옵니다.

    // 'pending', 'deactivated', 'disapproval' 상태의 사용자에게는 가격을 숨깁니다.
    if (in_array($user_group, ['pending', 'disapproval', 'deactivated', 'must_vendor'])) {
      return ''; // 가격 정보를 숨깁니다.
    }
  }

  return $price; // 그 외의 경우에는 가격 정보를 그대로 반환합니다.
}
add_filter('woocommerce_get_price_html', 'custom_hide_price_logic', 20); // 'woocommerce_get_price_html' 필터에 함수를 연결합니다.


// 비로그인 사용자에게 상품 카테고리를 숨기는 함수입니다.
function hide_category_for_unauthorized($terms, $post, $taxonomy)
{
  // 'product_cat' 택소노미에 대해 로그인하지 않은 사용자와 크롤러에게는 카테고리 정보를 숨깁니다.
  if ($taxonomy === 'product_cat' && !is_user_logged_in() && !is_crawler()) {
    return array(); // 카테고리 정보를 숨깁니다.
  }
  return $terms; // 그 외의 경우에는 카테고리 정보를 그대로 반환합니다.
}
add_filter('get_the_terms', 'hide_category_for_unauthorized', 30, 3); // 'get_the_terms' 필터에 함수를 연결합니다.
