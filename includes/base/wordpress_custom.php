<?php
// 주문 상품 헤더에 'Brand' 컬럼 추가
add_filter('woocommerce_admin_order_item_headers', 'add_order_item_brand_column_header');
function add_order_item_brand_column_header($order) {
    ?>
    <th class="item_brand"><?php _e('Brand', 'your-textdomain'); ?></th>
    <?php
}
// 주문 상품에 'Brand' 컬럼 값 추가
add_action('woocommerce_admin_order_item_values', 'add_order_item_brand_column_content', 10, 3);
function add_order_item_brand_column_content($product, $item, $item_id) {
    // 주문 항목이 상품인지 확인합니다.
    if ( ! is_a( $item, 'WC_Order_Item_Product' ) ) {
        echo '<td class="item_brand"></td>'; // 상품이 아닌 경우 빈 칼럼을 출력하고 함수를 종료합니다.
        return;
    }

    // 상품 ID를 가져옵니다.
    $product_id = $item->get_product_id();
    
    // 'product_brand' 타입소노미를 사용하여 상품의 브랜드를 조회합니다.
    $terms = wp_get_post_terms($product_id, 'product_brand');
    
    // 브랜드 이름을 가져옵니다. 여러 브랜드가 있을 수 있으니 첫 번째 브랜드를 사용합니다.
    $brand_name = !empty($terms) && !is_wp_error($terms) ? $terms[0]->name : '';

    ?>
    <td class="item_brand">
        <?php echo esc_html($brand_name); ?>
    </td>
    <?php
}

// 특정 클래스값을 가진 메뉴 숨기기
function filter_wp_nav_menu_objects($items, $args) {
    // 로그인한 사용자만 특정 메뉴 아이템을 보여줍니다.
    if (!is_user_logged_in()) {
        foreach ($items as $key => $item) {
            // 'private-item' 클래스를 가진 메뉴 아이템을 숨깁니다.
            if (in_array('must-hide', $item->classes)) {
                unset($items[$key]);
            }
        }
    }
    return $items;
}
add_filter('wp_nav_menu_objects', 'filter_wp_nav_menu_objects', 10, 2);

// Add custom CSS to WooCommerce emails
function custom_woocommerce_email_styles($css)
{
  $custom_css = '
        #template_header_image img {
            float: right;
            width: 200px;
        }
    ';

  return $css . $custom_css;
}
add_filter('woocommerce_email_styles', 'custom_woocommerce_email_styles', 999, 1);

//Search Engines에서 상품 가격 숨기기
add_filter('woocommerce_structured_data_product_offer', '__return_empty_array');

// Order View 페이지에서 상품 썸네일 표시
add_filter('woocommerce_order_item_name', 'display_product_image_in_order_item', 20, 3);
function display_product_image_in_order_item($item_name, $item, $is_visible)
{
  // Targeting view order pages only
  if (is_wc_endpoint_url('view-order')) {
    if ($item->get_product()) {
      $product = $item->get_product(); // Get the WC_Product object (from order item)
      $thumbnail = $product->get_image(array(80, 80)); // Get the product thumbnail (from product object)
      if ($product->get_image_id() > 0)
        $item_name = '<div class="item-thumbnail">' . $thumbnail . '</div>' . $item_name;
    } else {
      $item_name = '<div class="item-thumbnail"><img width="80" height="80" src="' . wc_placeholder_img_src() . '" class="attachment-80x80 size-80x80" alt="" loading="lazy"></div>' . $item_name;
    }
  }
  return $item_name;
}

// Rankmath 사이트맵 캐쉬 바이패스
// add_filter( 'rank_math/sitemap/enable_caching', '__return_false');

//썸네일 자동생성 중지
add_filter('woocommerce_background_image_regeneration', '__return_false');

// Regenerate the product attributes lookup table 사이즈 증가 //
add_filter('woocommerce_attribute_lookup_regeneration_step_size', function () {
  return 2000;
});

// 이미지 관련 코드
function remove_default_image_sizes( $sizes ) {
    unset( $sizes['thumbnail']); // 썸네일 사이즈 제거
    unset( $sizes['medium']);    // 중간 사이즈 제거
    unset( $sizes['large']);     // 대형 사이즈 제거
    // unset( $sizes['medium_large']); // 'medium_large' 사이즈를 제거하려면 이 줄의 주석을 해제하세요.
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');
add_filter('intermediate_image_sizes', '__return_empty_array', 99); // 추가 이미지 사이즈를 전혀 생성하지 않음

// WPrice 컬럼 추가하기 Products페이지

add_filter( 'manage_edit-product_columns', 'add_custom_product_column', 20 );
function add_custom_product_column( $columns ) {
    $new_columns = array();

    foreach ( $columns as $key => $title ) {
        $new_columns[ $key ] = $title;

        // 'Price' 컬럼 뒤에 새로운 'WPrice' 컬럼을 추가합니다.
        if ( $key == 'price' ) {
            $new_columns['wprice'] = __( 'WPrice', 'your-text-domain' );
        }
    }

    return $new_columns;
}
add_action( 'manage_product_posts_custom_column', 'show_custom_product_column_data', 10, 2 );
function show_custom_product_column_data( $column, $post_id ) {
    switch ( $column ) {
        case 'wprice':
            // '_custom_product_wholesale_price' 메타값을 가져와서 표시합니다.
            $wprice = get_post_meta( $post_id, '_custom_product_wholesale_price', true );
            echo wc_price( $wprice );
            break;
    }
}