<?php
// apM MUST 유저 관리 2024.01.31 베타테스트
// 사용자 프로필에 사용자 그룹 드롭다운 추가
add_action('show_user_profile', 'add_user_group_dropdown');
add_action('edit_user_profile', 'add_user_group_dropdown');

function add_user_group_dropdown($user)
{
  $user_group = get_the_author_meta('user_group', $user->ID);
  ?>
  <h3>B2B 유저 그룹관리</h3>
  <table class="form-table">
    <tr>
      <th><label for="user_group">유저 그룹 설정</label></th>
      <td>
        <select name="user_group" id="user_group">
          <option value="must_vendor" <?php selected($user_group, 'must_vendor'); ?>>Must Vendor</option>
          <option value="pending" <?php selected($user_group, 'pending'); ?>>Pending</option>
          <option value="disapproval" <?php selected($user_group, 'disapproval'); ?>>Disapproval</option>
          <option value="deactivated" <?php selected($user_group, 'deactivated'); ?>>Deactivated</option>
          <option value="temporary_global" <?php selected($user_group, 'temporary_global'); ?>>Temporary Global</option>
          <option value="temporary_asia" <?php selected($user_group, 'temporary_asia'); ?>>Temporary Asia</option>
          <option value="b2b_global" <?php selected($user_group, 'b2b_global'); ?>>B2B Global</option>
          <option value="b2b_asia" <?php selected($user_group, 'b2b_asia'); ?>>B2B Asia</option>
          <option value="b2b_ru" <?php selected($user_group, 'b2b_ru'); ?>>B2B Russia</option>
          <option value="b2b_kz_kg" <?php selected($user_group, 'b2b_kz_kg'); ?>>B2B KZ, KG</option>
          <option value="b2b_vip3" <?php selected($user_group, 'b2b_vip3'); ?>>B2B VIP3</option>
          <option value="b2b_vip5" <?php selected($user_group, 'b2b_vip5'); ?>>B2B VIP5</option>
        </select>
      </td>
    </tr>
  </table>
  <?php
}

// 사용자 프로필 저장 시 사용자 그룹 저장
add_action('personal_options_update', 'save_user_group');
add_action('edit_user_profile_update', 'save_user_group');

function save_user_group($user_id)
{
  if (!current_user_can('edit_user', $user_id)) {
    return false;
  }

  update_user_meta($user_id, 'user_group', $_POST['user_group']);
}

// 새 사용자가 가입할 때 기본 사용자 그룹을 'Pending'으로 설정
add_action('user_register', 'set_default_user_group');

function set_default_user_group($user_id)
{
  // 시간대를 'Asia/Seoul'로 설정 (KST)
  date_default_timezone_set('Asia/Seoul');

  // 기본 사용자 그룹을 'Pending'으로 설정
  update_user_meta($user_id, 'user_group', 'pending');

  // 현재 날짜와 시간을 'Y-m-d' 형식으로 가져오기 (KST 기준)
  $current_date_and_time = date('Y-m-d H:i:s');

  // user_group_update_date 메타 데이터에 현재 날짜와 시간 기록 (KST 기준)
  update_user_meta($user_id, 'user_group_update_date', $current_date_and_time);

  // 스크립트 실행 완료 후 기존의 시간대로 되돌리기 (필요한 경우)
  // date_default_timezone_set('원래의 시간대');
}

// 체크아웃 페이지에 서비스 요금 추가
add_action('woocommerce_cart_calculate_fees', 'add_custom_service_charge', 10, 1);

function add_custom_service_charge($cart)
{
  if (is_admin() && !defined('DOING_AJAX'))
    return;

  $user_group_fees = array(
    'pending' => array('service_charge' => 50), // Pending 그룹에 대한 서비스 요금
    'disapproval' => array('service_charge' => 50),
    'deactivated' => array('service_charge' => 50),
    'temporary_global' => array('service_charge' => 10),
    'temporary_asia' => array('service_charge' => 3),
    'b2b_global' => array('service_charge' => 10),
    'b2b_asia' => array('service_charge' => 3),
    'b2b_kz_kg' => array('service_charge' => 3),
    'b2b_ru' => array('service_charge' => 11),
    'b2b_vip3' => array('service_charge' => 10),
    'b2b_vip5' => array('service_charge' => 10),
  );

  if (is_user_logged_in()) {
    $user_id = get_current_user_id();
    $user_group = get_user_meta($user_id, 'user_group', true);

    if (array_key_exists($user_group, $user_group_fees)) {
      $service_charge_percentage = $user_group_fees[$user_group]['service_charge'];

      $service_charge = ($cart->subtotal * $service_charge_percentage) / 100;
      $cart->add_fee(__('Service Charge') . " ($service_charge_percentage%)", $service_charge, true);
    }
  }
}

// 체크아웃 페이지에서 결제 방법에 따른 추가 요금 설정
function add_gateway_surcharge_based_on_user_group($cart)
{
  if (is_admin() && !defined('DOING_AJAX'))
    return;

  // 초기 추가 요금 비율을 0으로 설정
  $surcharge_percentage = 0;

  // 사용자 그룹 및 결제 방법별 추가 요금 설정
  $user_group_gateway_surcharges = array(
    'pending' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
      // 필요에 따라 다른 결제 방법 및 요율 추가
    ),
    'disapproval' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
      // 필요에 따라 다른 결제 방법 및 요율 추가
    ),
    'deactivated' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
      // 필요에 따라 다른 결제 방법 및 요율 추가
    ),
    'temporary_global' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
      // 필요에 따라 다른 결제 방법 및 요율 추가
    ),
    'temporary_asia' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
      // 필요에 따라 다른 결제 방법 및 요율 추가
    ),
    'b2b_global' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
      // 필요에 따라 다른 결제 방법 및 요율 추가
    ),
    'b2b_asia' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
    ),
    'b2b_kz_kg' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
    ),
    'b2b_ru' => array(
      'nicepay_card' => 3.8,
      'ppcp-gateway' => 5.2,
    ),
    'b2b_vip3' => array(
      'nicepay_card' => 0,
      'ppcp-gateway' => 4.6,
    ),
    'b2b_vip5' => array(
      'nicepay_card' => 0,
      'ppcp-gateway' => 4,
    ),
  );

  if (is_user_logged_in()) {
    $user_id = get_current_user_id();
    $user_group = get_user_meta($user_id, 'user_group', true);
    $chosen_gateway = WC()->session->get('chosen_payment_method');

    if (isset($user_group_gateway_surcharges[$user_group][$chosen_gateway])) {
      $surcharge_percentage = $user_group_gateway_surcharges[$user_group][$chosen_gateway];
      $surcharge = ($cart->subtotal * $surcharge_percentage) / 100;

      // 게이트웨이 라벨 및 표시 비율 생성
      $gateway_label = '';
      switch ($chosen_gateway) {
        case 'nicepay_card':
          $gateway_label = 'Credit/Debit Card';
          break;
        case 'ppcp-gateway':
          $gateway_label = 'PayPal';
          break;
        // 다른 결제 게이트웨이에 대한 경우 추가...
      }
      $label = sprintf(__('%s Surcharge'), $gateway_label);
      $cart->add_fee($label, $surcharge, true);
    }
  }

  // 세션에 비율 저장
  WC()->session->set('payment_gateway_surcharge_percentage', $surcharge_percentage);
}
add_action('woocommerce_cart_calculate_fees', 'add_gateway_surcharge_based_on_user_group', 20, 1);


// SWIFT 결제에 대한 각 사용자 그룹별 할인 적용
function apply_swift_discount_based_on_user_group($cart)
{
  if (is_admin() && !defined('DOING_AJAX'))
    return;

  // 초기 할인 비율을 0으로 설정
  $discount_percentage = 0;

  // 사용자 그룹별 할인 비율을 key-value 쌍으로 배열에 저장
  $group_discounts = array(
    'pending' => 0,
    'disapproval' => 0,
    'deactivated' => 0,
    'temporary_global' => 0,
    'temporary_asia' => 0,
    'b2b_global' => 0,   	// B2B Global에 대해 0% 할인
    'b2b_asia' => 0,  		// B2B Asia에 대해 0% 할인
    'b2b_ru' => 0,  		// B2B Russia에 대해 0% 할인
    'b2b_kz_kg' => 0,  	// B2B KZ, KG에 대해 0% 할인
    'b2b_vip3' => 3, 		// B2B VIP3에 대해 3% 할인
    'b2b_vip5' => 5,    	// B2B VIP5에 대해 5% 할인
    // ...
  );

  if (is_user_logged_in()) {
    $user_id = get_current_user_id();
    $user_group = get_user_meta($user_id, 'user_group', true);

    if (array_key_exists($user_group, $group_discounts)) {

      // 선택된 결제 방법이 "bacs"인지 확인
      $chosen_payment_method = WC()->session->get('chosen_payment_method');

      if ($chosen_payment_method == 'bacs') {
        // 사용자 그룹에 따라 할인 계산
        $discount_percentage = $group_discounts[$user_group];
        $discount = ($cart->subtotal) * ($discount_percentage / 100);

        // 그룹 이름을 대문자로 포맷팅하여 라벨 준비
        $formatted_group = strtoupper(str_replace('_', ' ', $user_group));

        // 할인 적용
        $cart->add_fee(sprintf(__('%s SWIFT DISCOUNT %s%%', 'woocommerce'), $formatted_group, $discount_percentage), -$discount);
      }
    }
  }

  // 세션에 비율 저장
  WC()->session->set('swift_discount_percentage', $discount_percentage);
}
add_action('woocommerce_cart_calculate_fees', 'apply_swift_discount_based_on_user_group', 10, 1);

// 사용자 그룹에 따라 결제 수단 필터링
function filter_payment_gateways_based_on_user_group($available_gateways)
{
  if (!is_admin()) {
    if (is_user_logged_in()) {
      $user_id = get_current_user_id();
      $user_group = get_user_meta($user_id, 'user_group', true);

      // 각 사용자 그룹에 대한 결제 수단 설정
      $group_gateways = array(
        'b2b_global' => array('nicepay_card', 'bacs', 'ppcp-gateway'),
        'deactivated' => array('nicepay_card'),
        'temporary_global' => array('nicepay_card', 'bacs', 'ppcp-gateway'),
        'temporary_asia' => array('nicepay_card', 'bacs', 'ppcp-gateway'),
        'b2b_asia' => array('nicepay_card', 'bacs', 'ppcp-gateway'),
        'b2b_kz_kg' => array('nicepay_card', 'bacs', 'ppcp-gateway'),
        'b2b_ru' => array('bacs'),
        'b2b_vip3' => array('nicepay_card', 'bacs', 'ppcp-gateway'),
        'b2b_vip5' => array('nicepay_card', 'bacs', 'ppcp-gateway'),
        // ...
      );

      if (isset($group_gateways[$user_group])) {
        foreach ($available_gateways as $gateway_id => $gateway) {
          if (!in_array($gateway_id, $group_gateways[$user_group])) {
            unset($available_gateways[$gateway_id]);
          }
        }
      }
    }
  }

  return $available_gateways;
}
add_filter('woocommerce_available_payment_gateways', 'filter_payment_gateways_based_on_user_group', 10, 1);