<?php
// apM MUST 벤더 유저 관리 페이지
function register_vendor_management_page() {
  add_users_page(
      '벤더 관리', // 페이지 타이틀
      '벤더 관리', // 메뉴 타이틀
      'manage_options', // 필요한 권한 (이 예에서는 관리자 권한)
      'must_vendor_management', // 메뉴 슬러그
      'vendor_management_page_content' // 콜백 함수, 페이지 컨텐츠를 출력
  );
}
add_action('admin_menu', 'register_vendor_management_page');

// 마지막 상품 업로드일
function get_last_post_upload_date($user_id) {
  $last_upload_query = new WP_Query(array(
      'author' => $user_id,
      'post_type' => 'product', // 상품 포스트 타입 지정
      'posts_per_page' => 1, // 가장 최근 1개의 포스트만 가져옴
      'orderby' => 'date', // 날짜 순으로 정렬
      'order' => 'DESC', // 내림차순으로 최신 포스트부터
  ));

  $last_upload_date = 'X'; // 기본값 설정
  if ($last_upload_query->have_posts()) {
      $last_upload_query->the_post(); // 가장 최근 포스트 데이터 로드
      $post_date = get_the_date('U'); // 포스트의 Unix 타임스탬프를 가져오기
      $current_time = current_time('timestamp'); // 워드프레스 현재 시간의 Unix 타임스탬프
      $last_upload_date = human_time_diff($post_date, $current_time) . ' ago'; // x days ago 형식으로 변환
  }
  wp_reset_postdata(); // 글로벌 포스트 데이터 리셋

  return $last_upload_date;
}

function vendor_management_page_content() {
  // 현재 선택된 탭을 GET 파라미터에서 추출하거나 기본값 설정
  $current_tab = isset($_GET['tab']) ? $_GET['tab'] : 'all';

  $all_vendors = get_users(array(
      'role'    => 'apmmust_vendor',
      'orderby' => 'registered',
      'order'   => 'DESC'
  ));

  // 사용자 필터링 로직 개선
  $vendors = array_filter($all_vendors, function($user) {
      return !str_starts_with($user->user_email, 'test-');
  });

  $test_users = array_filter($all_vendors, function($user) {
      return str_starts_with($user->user_email, 'test-');
  });

  // 탭 출력
  echo '<h1>Vendor Management</h1>';
  echo '<ul class="subsubsub">';
  echo '<li><a href="?page=must_vendor_management&tab=all"'. ($current_tab == 'all' ? ' class="current"' : '') .'>All ('. count($all_vendors) .')</a> |</li>';
  echo '<li><a href="?page=must_vendor_management&tab=vendor"'. ($current_tab == 'vendor' ? ' class="current"' : '') .'>Vendor ('. count($vendors) .')</a> |</li>';
  echo '<li><a href="?page=must_vendor_management&tab=test"'. ($current_tab == 'test' ? ' class="current"' : '') .'>Test ('. count($test_users) .')</a></li>';
  echo '</ul>';

  // 컬럼 정보를 배열로 정의
  $columns = array(
      'Email' => function($vendor) { return '<a href="' . get_edit_user_link($vendor->ID) . '">' . $vendor->user_email . '</a>'; },
      'Company' => function($vendor) { return get_user_meta($vendor->ID, 'temp_vendor_store_brand', true); },
      'Building' => function($vendor) { return get_user_meta($vendor->ID, 'temp_vendor_store_building_term', true); },
      '가입일' => function($vendor) { return date('Y-m-d', strtotime($vendor->user_registered)); },
      '상품 업로드일' => function($vendor) { return get_last_post_upload_date($vendor->ID); },
      '총 상품 개수' => function($vendor) { return count_user_posts($vendor->ID, 'product', true); },
  );

  // 선택된 탭에 따라 사용자 목록 필터링
  switch ($current_tab) {
      case 'vendor':
          $filtered_users = $vendors;
          break;
      case 'test':
          $filtered_users = $test_users;
          break;
      case 'all':
      default:
          $filtered_users = $all_vendors;
          break;
  }

  echo '<table class="wp-list-table widefat fixed striped">';
  echo '<thead><tr>';
  foreach ($columns as $column_name => $callback) {
      echo "<th>{$column_name}</th>";
  }
  echo '</tr></thead>';
  echo '<tbody>';
  foreach ($filtered_users as $vendor) {
      echo '<tr>';
      foreach ($columns as $callback) {
          echo '<td>' . $callback($vendor) . '</td>';
      }
      echo '</tr>';
  }
  echo '</tbody></table>';
}
