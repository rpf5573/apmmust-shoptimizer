<?php
// apM MUST 유저 관리 2024.02.07 베타테스트
// Deactivated, Temporary 유저그룹 관리 최적화
// 서버크론으로 PHP-cli로 실행
// 테스트목적 크론 
// * * * * * /usr/bin/php /var/www/html/wp-content/themes/shoptimizer-child-theme/includes/user/user_deactivate_temporary.php >/dev/null 2>&1
// Production 환경용 크론
// 20 12 * * * /usr/bin/php /var/www/html/wp-content/themes/shoptimizer-child-theme/includes/user/user_deactivate_temporary.php >/dev/null 2>&1
// 20 12 * * * /usr/bin/php /var/www/html/wp-content/themes/shoptimizer-child-theme/includes/user/user_deactivate_temporary.php >> /var/www/manual_cron.log 2>&1

// 서버크론 실행을 위해 워드프레스 환경 불러오기
require_once('/var/www/html/wp-load.php');

// 사용자 데이터 검색
function find_users_by_group_and_role() {
  // 원하는 사용자 그룹
  $user_groups = ['b2b_global', 'b2b_asia', 'b2b_ru', 'b2b_kz_kg', 'temporary_global', 'temporary_asia'];

  // meta_query 초기화
  $meta_query = array('relation' => 'OR');

  // 각 사용자 그룹에 대한 meta_query 구성
  foreach ($user_groups as $group) {
      $meta_query[] = array(
          'key' => 'user_group',
          'value' => $group,
          'compare' => '='
      );
  }

  // WP_User_Query 실행
  $args = array(
      'role' => 'customer',
      'meta_query' => $meta_query
  );

  $user_query = new WP_User_Query($args);
  $users = $user_query->get_results();

  return $users;
}


// 경과일 계산
function calculate_days_passed($date_from)
{
  try {
    $date_from = new DateTime($date_from);
  } catch (Exception $e) {
    // 날짜 생성 실패 시 로그를 남기고 0을 반환
    error_log("Failed to parse date: " . $e->getMessage());
    return 0;
  }
  $date_now = new DateTime();
  $interval = $date_now->diff($date_from);
  return $interval->days;
}


// WooCommerce에서 사용자의 마지막 주문 검색
function get_last_order_date($user_id)
{
  $args = array(
    'customer_id' => $user_id,
    // 'completed'와 'processing' 상태의 주문을 모두 포함
    'status' => array('completed', 'processing'),
    'orderby' => 'date',
    'order' => 'DESC',
    'limit' => 1,
  );

  $orders = wc_get_orders($args);
  if (!empty($orders)) {
    $last_order = $orders[0];
    return $last_order->get_date_created()->date('Y-m-d');
  }

  // 주문이 없는 경우 로그를 남기고 null 반환
  return null;
}


// 사용자 배열을 받아 각 사용자의 user_group_update_date 및 마지막 주문일로부터의 경과일을 계산
function check_users($users)
{
  foreach ($users as $user) {
    $user_id = $user->ID;

    // 사용자 ID의 유효성을 확인합니다.
    if (!$user_id || !get_user_by('id', $user_id)) {
      // 유효하지 않은 사용자 ID에 대한 처리 (예: 로깅)
      error_log("Invalid user ID: $user_id");
      continue; // 다음 사용자로 넘어갑니다.
    }

    $user_group_update_date = get_user_meta($user_id, 'user_group_update_date', true);

    // user_group_update_date의 존재 여부를 확인합니다.
    if (!$user_group_update_date) {
      // user_group_update_date 메타 데이터가 없는 경우의 처리 (예: 로깅)
      error_log("Missing user_group_update_date for user ID: $user_id");
      continue; // 다음 사용자로 넘어갑니다.
    }

    $days_since_group_update = calculate_days_passed($user_group_update_date);

    $last_order_date = get_last_order_date($user_id);
    $days_since_last_order = $last_order_date ? calculate_days_passed($last_order_date) : null; // 주문이 없는 경우 null로 처리

    // 사용자 그룹 업데이트 로직을 호출합니다.
    update_user_group_based_on_conditions($user, $days_since_group_update, $days_since_last_order);
  }
}


// 조건에 따른 user_group 업데이트
function update_user_group_based_on_conditions($user, $days_since_group_update, $days_since_last_order)
{
  $user_id = $user->ID;
  $user_group = get_user_meta($user_id, 'user_group', true);

  // b2b 관련 그룹인 경우
  if (in_array($user_group, ['b2b_global', 'b2b_asia', 'b2b_ru', 'b2b_kz_kg'])) {
    // 첫 번째 조건: 최근 주문일이 120일 이상인 경우
    if ($days_since_last_order > 120) {
      error_log("User ID {$user_id} is being changed to deactivated due to Condition 1 (the order being more than 120 days old).");
      change_user_group($user_id, 'deactivated');
  }
    // 두 번째 조건: user_group_update_date가 120일 초과하고, 아예 주문 이력이 없는 경우
    else if ($days_since_group_update > 120 && $days_since_last_order === null) {
      error_log("User ID {$user_id} is being changed to deactivated due to Condition 2 (user_group_update_date exceeded 120 days with no order history).");
      change_user_group($user_id, 'deactivated');
    }
  }

  // temporary 관련 그룹인 경우
  elseif (in_array($user_group, ['temporary_global', 'temporary_asia'])) {
    // 조건1: user_group_update_date가 30일 초과할 경우 deactivated
    if ($days_since_group_update > 30) {
      error_log("User ID {$user_id} is being changed to 'deactivated' due to Condition 1 (user_group_update_date exceeded 30 days).");
      change_user_group($user_id, 'deactivated');
    }
    // 조건2: user_group_update_date가 30일 이내이고 최근 주문일도 30일 이내일 경우
    elseif ($days_since_group_update <= 30 && ($days_since_last_order !== null && $days_since_last_order <= 30)) {
      if ($user_group === 'temporary_global') {
        error_log("User ID {$user_id} is being changed to 'b2b_global' due to Condition 2 (user_group_update_date and last_order_date within 30 days).");
        change_user_group($user_id, 'b2b_global');
      } elseif ($user_group === 'temporary_asia') {
        error_log("User ID {$user_id} is being changed to 'b2b_asia' due to Condition 2 (user_group_update_date and last_order_date within 30 days).");
        change_user_group($user_id, 'b2b_asia');
      }
    }
  }
}

// deactivation_count와 user_group_update_date 업데이트
function change_user_group($user_id, $new_group)
{
  // 현재 user_group을 가져옵니다.
  $current_group = get_user_meta($user_id, 'user_group', true);

  // 새 그룹이 현재 그룹과 다를 경우에만 업데이트 진행
  if ($current_group != $new_group) {
    // user_group 업데이트
    $update_result = update_user_meta($user_id, 'user_group', $new_group);
    if ($update_result) {
      error_log("User group updated successfully for user ID {$user_id} to {$new_group}");
    } else {
      error_log("Failed to update user group for user ID {$user_id} to {$new_group}");
    }

    // user_group이 'deactivated'로 변경되고 이전에 'deactivated' 상태가 아니었을 경우에만 deactivation_count 업데이트
    if ($new_group == 'deactivated' && $current_group != 'deactivated') {
      $deactivation_count = (int) get_user_meta($user_id, 'deactivation_count', true);
      $update_count_result = update_user_meta($user_id, 'deactivation_count', ++$deactivation_count);
      if ($update_count_result) {
        error_log("Deactivation count updated successfully for user ID {$user_id}. New count: {$deactivation_count}");
      } else {
        error_log("Failed to update deactivation count for user ID {$user_id}");
      }
    }

    // user_group_update_date 업데이트 - 날짜와 시간 포함
    $update_date_result = update_user_meta($user_id, 'user_group_update_date', current_time('Y-m-d H:i:s'));
    if ($update_date_result) {
      error_log("User group update date set successfully for user ID {$user_id}");
    } else {
      error_log("Failed to set user group update date for user ID {$user_id}");
    }
  } else {
    // 만약 변경할 그룹과 현재 그룹이 같을 경우, 로그 기록
    error_log("No change in user group for user ID {$user_id}. Attempted to set to {$new_group}, which is already the current group.");
  }
}

// 서버크론을 위한 함수 호출
deactivation_apm_user_group_management();

function deactivation_apm_user_group_management() {
  // 사용자 데이터 검색
  $users_to_check = find_users_by_group_and_role();

  // 검색된 사용자에 대한 처리
  if (!empty($users_to_check)) {
      error_log("Starting user group management...");
      check_users($users_to_check);
      error_log("Deactivation User group management completed.");
  } else {
      error_log("No users found for the specified groups.");
  }
  
  // 크론 작업 완료 로그를 남깁니다.
  $current_time = current_time('Y-m-d H:i:s'); // 워드프레스의 current_time 함수를 사용
  error_log("Deactivation Cron Job has been done at {$current_time}\n", 3, "/var/www/manual_cron.log");
}


