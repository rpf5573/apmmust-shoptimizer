<?php
// apM MUST Register페이지 추가 필드 2024.01.31
// Form태그 설정
function custom_add_enctype_to_registration_form()
{
  echo 'enctype="multipart/form-data"'; // 이를 폼 태그에 추가
}
add_action('woocommerce_register_form_tag', 'custom_add_enctype_to_registration_form');

// 우커머스 작동 여부 확인
if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))))
  return;

// Add custom fields to WooCommerce registration form
function custom_woocommerce_register_form_fields()
{
  // Enqueue WooCommerce scripts for country/state dropdown
  wp_enqueue_script('wc-country-select');

  // Country field
  woocommerce_form_field(
    'billing_country',
    array(
      'type' => 'country',
      'class' => array('form-row-wide', 'address-field', 'update_totals_on_change', 'register-must'),
      'label' => __('Country', 'woocommerce'),
      'required' => true,
      'default' => isset($_POST['billing_country']) ? wc_clean($_POST['billing_country']) : '',
    )
  );

  // First Name
  woocommerce_form_field(
    'billing_first_name',
    array(
      'type' => 'text',
      'class' => array('form-row-first', 'register-must'),
      'label' => __('First Name', 'woocommerce'),
      'required' => true,
      'default' => isset($_POST['billing_first_name']) ? wc_clean($_POST['billing_first_name']) : '',
    )
  );

  // Last Name
  woocommerce_form_field(
    'billing_last_name',
    array(
      'type' => 'text',
      'class' => array('form-row-last', 'register-must'),
      'label' => __('Last Name', 'woocommerce'),
      'required' => true,
      'default' => isset($_POST['billing_last_name']) ? wc_clean($_POST['billing_last_name']) : '',
    )
  );

  // Company Name
  woocommerce_form_field(
    'billing_company',
    array(
      'type' => 'text',
      'class' => array('form-row-wide', 'register-must'),
      'label' => __('Company Name', 'woocommerce'),
      'required' => true,
      'default' => isset($_POST['billing_company']) ? wc_clean($_POST['billing_company']) : '',
    )
  );

  // Phone
  woocommerce_form_field(
    'billing_phone',
    array(
      'type' => 'tel',
      'class' => array('form-row-wide', 'register-must'),
      'label' => __('Phone', 'woocommerce'),
      'required' => true,
      'default' => isset($_POST['billing_phone']) ? wc_clean($_POST['billing_phone']) : '+',
      'custom_attributes' => array(
        'minlength' => '6'  // 최소 8자리 필요
      )
    )
  );

  // Shop URL with https:// prefilled
  woocommerce_form_field(
    'additional_shop_url',
    array(
      'type' => 'text',
      'class' => array('form-row-wide', 'register-must'),
      'label' => __('Shop URL', 'woocommerce'),
      'required' => true,
      'input_class' => array('input-text'),
      'placeholder' => __('yourshop.com', 'woocommerce'),
      'default' => isset($_POST['additional_shop_url']) ? wc_clean($_POST['additional_shop_url']) : 'https://',
    )
  );

  // Business License File Upload Field   Supported file types: jpg, jpeg, png, txt, pdf, doc, docx
  ?>
  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide register-must">
    <label for="b2b_business_license_file" class="required">
      <?php _e('Business License', 'woocommerce'); ?> <abbr class="required" title="required">*</abbr>
      <span class="file-types">
        <?php _e('(Supported file types: jpg, jpeg, png, txt, pdf, doc, docx)', 'woocommerce'); ?>
      </span>
    </label>
    <input type="file" class="woocommerce-Input woocommerce-Input--text input-text" name="b2b_business_license_file"
      id="b2b_business_license_file" accept=".png, .jpeg, .jpg, .pdf, .docx, .doc, .webp" required>
  </p>


  <?php

  // WhatsApp
  woocommerce_form_field(
    'additional_whatsapp',
    array(
      'type' => 'tel',
      'class' => array('form-row-wide'),
      'label' => __('WhatsApp', 'woocommerce'),
      'required' => false,
      'placeholder' => 'Highly recommeded to contact us.',
      'default' => isset($_POST['additional_whatsapp']) ? wc_clean($_POST['additional_whatsapp']) : '',
    )
  );

  // VAT No.
  woocommerce_form_field(
    'billing_vat',
    array(
      'type' => 'text',
      'class' => array('form-row-wide'),
      'label' => __('VAT No.', 'woocommerce'),
      'required' => false,
      'default' => isset($_POST['billing_vat']) ? wc_clean($_POST['billing_vat']) : '',
    )
  );

  // EORI No.
  woocommerce_form_field(
    'billing_eori',
    array(
      'type' => 'text',
      'class' => array('form-row-wide'),
      'label' => __('EORI No.', 'woocommerce'),
      'required' => false,
      'placeholder' => 'A client from EU and GB should fill in your EORI or IOSS Number for custom clearance.',
      'default' => isset($_POST['billing_eori']) ? wc_clean($_POST['billing_eori']) : '',
    )
  );

}

add_action('woocommerce_register_form_start', 'custom_woocommerce_register_form_fields');

// Ensure that WordPress allows file uploads
function custom_woocommerce_allow_business_license_uploads($file)
{
  $file['test_form'] = false;
  return $file;
}
add_filter('wp_handle_upload_prefilter', 'custom_woocommerce_allow_business_license_uploads');

// 파일업로드 클라이언트측 자바스크립트 Validation
function custom_woocommerce_business_license_script()
{
  ?>
  <script type="text/javascript">
    jQuery(document).ready(function ($) {
      $('form.woocommerce-form-register').on('submit', function (e) {
        var allowedExtensions = /(\.png|\.jpeg|\.jpg|\.pdf|\.docx|\.doc|\.webp)$/i;
        var licenseFile = $('#b2b_business_license_file')[0].files[0];

        if (licenseFile) {
          // 파일 확장자 검사
          if (!allowedExtensions.exec(licenseFile.name)) {
            alert('Error: Invalid file type. Allowed types are .png, .jpeg, .jpg, .pdf, .docx, .doc, .webp');
            e.preventDefault();
            return; // 추가된 코드
          }
          // 파일 크기 검사
          if (licenseFile.size > 5242880) { // 5MB in bytes
            alert('Error: Your file is larger than 5MB');
            e.preventDefault();
          }
        }
      });
    });
  </script>
  <?php
}

add_action('woocommerce_register_form', 'custom_woocommerce_business_license_script');

// 파일 업로드 처리를 위한 서버측 Validation
function custom_handle_business_license_upload($customer_id)
{
  if (isset($_FILES['b2b_business_license_file']) && $_FILES['b2b_business_license_file']['size'] > 0) {
    require_once(ABSPATH . 'wp-admin/includes/file.php'); // WP file API

    // 파일 크기 검사 (5MB 이상인 경우 에러 처리)
    if ($_FILES['b2b_business_license_file']['size'] > 5242880) { // 5MB in bytes
      // 적절한 사용자 알림 처리
      wp_die('Error: File size exceeds the 5MB limit.');
    }

    // 파일 업로드 옵션 설정
    $upload_overrides = array('test_form' => false);
    $uploaded_file = wp_handle_upload($_FILES['b2b_business_license_file'], $upload_overrides);

    if (isset($uploaded_file['file'])) {
      // 파일이 성공적으로 업로드되었을 경우
      $file_name = basename($uploaded_file['file']);
      $file_type = wp_check_filetype($uploaded_file['file']);

      // 파일 타입 검증 (png, jpeg, jpg, pdf, docx, doc, webp)
      $allowed_file_types = array('png', 'jpeg', 'jpg', 'pdf', 'docx', 'doc', 'webp');
      if (in_array($file_type['ext'], $allowed_file_types)) {
        // 사용자 메타 데이터에 파일 경로 저장
        update_user_meta($customer_id, 'b2b_business_license_file', $uploaded_file['url']);
      } else {
        // 잘못된 파일 형식 에러 처리
        wp_die('Error: Invalid file type. Allowed types are .png, .jpeg, .jpg, .pdf, .docx, .doc, .webp');
      }
    } else {
      // 파일 업로드 실패 에러 처리
      wp_die('Error: File upload failed. Please try again.');
    }
  } else {
    // 파일이 전혀 업로드되지 않았을 경우의 에러 처리
    wp_die('Error: No file uploaded. Please upload a file.');
  }
}
add_action('woocommerce_created_customer', 'custom_handle_business_license_upload');

// 서버 측에서 필수 필드 유효성 검사
function custom_validate_registration_form_fields($username, $email, $validation_errors)
{
  if (isset($_POST['billing_country']) && empty($_POST['billing_country'])) {
    $validation_errors->add('billing_country_error', __('Country is a required field.', 'woocommerce'));
  }

  if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {
    $validation_errors->add('billing_first_name_error', __('First name is a required field.', 'woocommerce'));
  }

  if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {
    $validation_errors->add('billing_last_name_error', __('Last name is a required field.', 'woocommerce'));
  }

  if (isset($_POST['billing_company']) && empty($_POST['billing_company'])) {
    $validation_errors->add('billing_company_error', __('Company name is a required field.', 'woocommerce'));
  }

  if (isset($_POST['billing_phone']) && empty($_POST['billing_phone'])) {
    $validation_errors->add('billing_phone_error', __('Phone is a required field.', 'woocommerce'));
  }

  // 전체 필드 검사 후 오류가 있으면 사용자에게 알립니다.
  if (!empty($validation_errors->get_error_messages())) {
    throw new Exception(implode('<br/>', $validation_errors->get_error_messages()));
  }
}

add_action('woocommerce_register_post', 'custom_validate_registration_form_fields', 10, 3);



// Save custom fields in user meta on registration
function custom_save_woocommerce_register_fields($customer_id)
{
  if (isset($_POST['billing_country'])) {
    // Save Country
    update_user_meta($customer_id, 'billing_country', sanitize_text_field($_POST['billing_country']));
  }

  if (isset($_POST['billing_first_name'])) {
    // Save First Name to both billing_first_name and first_name fields
    $first_name = sanitize_text_field($_POST['billing_first_name']);
    update_user_meta($customer_id, 'billing_first_name', $first_name);
    // Additionally save to first_name for user's profile
    update_user_meta($customer_id, 'first_name', $first_name);
  }

  if (isset($_POST['billing_last_name'])) {
    // Save Last Name to both billing_last_name and last_name fields
    $last_name = sanitize_text_field($_POST['billing_last_name']);
    update_user_meta($customer_id, 'billing_last_name', $last_name);
    // Additionally save to last_name for user's profile
    update_user_meta($customer_id, 'last_name', $last_name);
  }

  if (isset($_POST['billing_company'])) {
    // Save Company Name
    update_user_meta($customer_id, 'billing_company', sanitize_text_field($_POST['billing_company']));
  }

  if (isset($_POST['billing_phone'])) {
    // Save Phone
    update_user_meta($customer_id, 'billing_phone', sanitize_text_field($_POST['billing_phone']));
  }

  if (isset($_POST['additional_whatsapp'])) {
    // Save WhatsApp
    update_user_meta($customer_id, 'additional_whatsapp', sanitize_text_field($_POST['additional_whatsapp']));
  }

  if (isset($_POST['billing_vat'])) {
    // Save VAT No.
    update_user_meta($customer_id, 'billing_vat', sanitize_text_field($_POST['billing_vat']));
  }

  if (isset($_POST['billing_eori'])) {
    // Save EORI No.
    update_user_meta($customer_id, 'billing_eori', sanitize_text_field($_POST['billing_eori']));
  }

  if (isset($_POST['additional_shop_url'])) {
    // Save Shop URL
    update_user_meta($customer_id, 'additional_shop_url', sanitize_text_field($_POST['additional_shop_url']));
  }
}

add_action('woocommerce_created_customer', 'custom_save_woocommerce_register_fields');
