<?php
// apM MUST 유저 관리 2024.02.05 베타테스트
// 사용자 관리 페이지
function must_user_management_menu()
{
    add_users_page(
        '고객 관리',
        '고객 관리',
        'manage_options',
        'must-user-management',
        'must_user_management_page_callback'
    );
}

// Pending 사용자 뱃지로 표시하기
// 사용자 수 계산하기
function count_pending_user_groups()
{
    $args = array(
        'role' => 'Customer', // Customer 역할을 가진 사용자만 대상으로 합니다.
        'meta_query' => array(
            array(
                'key' => 'user_group',
                'value' => 'pending',
                'compare' => '='
            )
        ),
        'count_total' => true,
    );

    $user_query = new WP_User_Query($args);
    return $user_query->get_total();
}

// 어드민 메뉴 항목에 뱃지 추가하기
function add_pending_users_badge_to_menu()
{
    $pending_count = count_pending_user_groups(); // pending 상태인 사용자 수 계산
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var menu = $('a[href="users.php?page=must-user-management"]'); // 메뉴 항목 선택
            if (menu.length) {
                var badge = '<span class="update-plugins count-' + <?php echo $pending_count; ?> + '" title="Pending Users"><span class="pending-count">' + <?php echo $pending_count; ?> + '</span></span>';
                menu.append(badge); // 메뉴 항목에 뱃지 추가
            }
        });
    </script>
    <?php
}
add_action('admin_footer', 'add_pending_users_badge_to_menu');

// WC_Countries 인스턴스를 반환하는 함수
function get_wc_countries_instance()
{
    static $countries_instance = null; // 정적 변수를 사용하여 인스턴스를 한 번만 생성
    if (null === $countries_instance) {
        $countries_instance = new WC_Countries();
    }
    return $countries_instance;
}

// 국가 코드를 국가 이름으로 변환하는 함수
function get_country_name_by_code($country_code)
{
    $countries = get_wc_countries_instance(); // 수정된 부분
    $country_names = $countries->get_countries();
    return isset($country_names[$country_code]) ? $country_names[$country_code] : $country_code;
}


// 국가 이름을 국가 코드로 변환하는 함수
function convert_country_name_to_code($country_name)
{
    $countries = get_wc_countries_instance(); // 수정된 부분
    $countries_array = $countries->get_countries();
    foreach ($countries_array as $code => $name) {
        if (strtolower($name) == strtolower($country_name)) {
            return $code;
        }
    }
    return false; // 국가 이름이 목록에 없는 경우
}

// 국가 목록 쿼리
function get_unique_countries_from_users()
{
    global $wpdb;

    // 캐시에서 국가 목록을 시도하여 가져옵니다.
    $countries = wp_cache_get('unique_countries_from_users', 'apm_must_user_management');

    // 캐시에 데이터가 없는 경우, 데이터베이스 쿼리를 실행합니다.
    if (false === $countries) {
        $query = "SELECT DISTINCT meta_value FROM {$wpdb->usermeta} WHERE meta_key = 'billing_country' AND meta_value != '' ORDER BY meta_value ASC";
        $country_codes = $wpdb->get_col($query);

        $countries = array();
        foreach ($country_codes as $code) {
            $countries[$code] = get_country_name_by_code($code);
        }

        // 쿼리 결과를 캐시에 저장합니다. 캐시 유지 시간은 필요에 따라 조정할 수 있습니다.
        wp_cache_set('unique_countries_from_users', $countries, 'apm_must_user_management', 24 * 60 * 60); // 1일 동안 캐시
    }

    return $countries;
}


// 사용자가 가입한 날짜부터 오늘까지의 개월 수를 계산하는 함수
function get_months_since_registration($registration_date)
{
    $registered = new DateTime($registration_date);
    $now = new DateTime();
    $interval = $registered->diff($now);

    // 연도를 개월 수로 변환하고 현재 연도의 개월 수를 더합니다.
    $months = $interval->y * 12 + $interval->m;
    return $months;
}

// 유저의 최근 주문 일 계산
function get_last_order_date_for_user($user_id)
{
    // 캐시에서 유저의 최근 주문 날짜를 시도하여 가져옵니다.
    $cache_key = 'last_order_date_for_user_' . $user_id;
    $cached = wp_cache_get($cache_key, 'apm_must_user_management');

    if (false !== $cached) {
        // 캐시된 값이 있으면 바로 반환합니다.
        return $cached;
    }

    $args = array(
        'numberposts' => 1,
        'meta_key' => '_customer_user',
        'meta_value' => $user_id,
        'post_type' => wc_get_order_types(),
        // 'completed'와 'processing' 상태의 주문만 포함시킵니다.
        'post_status' => array('wc-completed', 'wc-processing'),
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $last_order = get_posts($args);

    if ($last_order) {
        $last_order_date = date("Y-m-d H:i:s", strtotime($last_order[0]->post_date));
        $days_diff = round((time() - strtotime($last_order_date)) / (60 * 60 * 24));

        if ($days_diff >= 120) {
            $output = '<strong style="color: red;">' . date("Y-m-d", strtotime($last_order_date)) . ' / ' . $days_diff . ' days ago</strong>';
        } else {
            $output = date("Y-m-d", strtotime($last_order_date)) . ' / ' . $days_diff . ' days ago';
        }
    } else {
        $output = '<strong style="color: red;">No order yet</strong>';
    }

    // 결과를 캐시에 저장합니다. 캐시 유지 시간을 1일(24 * 60 * 60 초)로 설정합니다.
    wp_cache_set($cache_key, $output, 'apm_must_user_management', 24 * 60 * 60); // 하루 동안 캐시

    return $output;
}

// 캐싱 무효화 로직
add_action('woocommerce_order_status_changed', 'invalidate_last_order_date_cache', 10, 3);

function invalidate_last_order_date_cache($order_id, $old_status, $new_status)
{
    // 주문 객체를 가져옵니다.
    $order = wc_get_order($order_id);
    if ($order) {
        // 주문에서 사용자 ID를 가져옵니다.
        $user_id = $order->get_customer_id();
        if ($user_id) {
            // 특정 사용자의 최근 주문일 캐시 키를 구성합니다.
            $cache_key = 'last_order_date_for_user_' . $user_id;
            // 해당 캐시를 삭제합니다.
            wp_cache_delete($cache_key, 'apm_must_user_management');
        }
    }
}

// 테이블 헤더에 정렬 링크 추가
function must_user_management_sortable_header($header, $orderby_key, $current_orderby, $current_order, $current_page, $country_filter, $search_query)
{
    $order = $current_orderby === $orderby_key && $current_order === 'asc' ? 'desc' : 'asc';
    $link = add_query_arg(
        array(
            'page' => 'must-user-management',
            'orderby' => $orderby_key,
            'order' => $order,
            'paged' => $current_page, // 현재 페이지 번호
            'country_filter' => $country_filter, // 국가 필터
            's' => $search_query // 검색어
        ),
        admin_url('admin.php')
    );
    echo '<th><a href="' . esc_url($link) . '">' . esc_html($header) . '</a></th>';
}

// 정렬 가능한 컬럼
function must_user_management_sortable_columns($sortable_columns)
{
    $sortable_columns['user_group_update_date'] = 'user_group_update_date';
    $sortable_columns['deactivation_count'] = 'deactivation_count';
    return $sortable_columns;
}
add_filter('manage_users_sortable_columns', 'must_user_management_sortable_columns');

// 정렬을 위한 사용자 쿼리 수정
function must_user_management_orderby_custom_columns($query)
{
    if (!is_admin()) {
        return;
    }

    $orderby = $query->get('orderby');

    // 유저그룹 갱신일 정렬 조건
    if ('user_group_update_date' == $orderby) {
        $query->set('meta_key', 'user_group_update_date');
        $query->set('orderby', 'meta_value');
    }
    // 비활성화 횟수(deactivation_count) 정렬 조건
    if ('deactivation_count' == $orderby) {
        $query->set('meta_key', 'deactivation_count');
        $query->set('orderby', 'meta_value_num'); // 숫자 정렬을 위해 meta_value_num 사용
    }
}
add_action('pre_get_users', 'must_user_management_orderby_custom_columns');

// 머스트 매니지먼트 페이지 콜백함수
function must_user_management_page_callback()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    // 폼 제출 검증
    if (isset($_POST['action']) && $_POST['action'] != '-1') {
        // Nonce 검증 추가
        if (!isset($_POST['must_user_management_nonce']) || !wp_verify_nonce($_POST['must_user_management_nonce'], 'bulk-user-group-change')) {
            wp_die('Nonce verification failed', 'Invalid Request', array('response' => 403));
        }

        // 선택된 사용자들의 배열 확인 및 정수 변환
        $user_ids = isset($_POST['users']) ? array_map('intval', $_POST['users']) : array();
        $action = isset($_POST['action']) ? sanitize_text_field($_POST['action']) : '';

        // 유효한 액션 확인
        $valid_actions = ['pending', 'disapproval', 'b2b_global', 'b2b_asia', 'b2b_kz_kg', 'b2b_ru', 'b2b_vip3', 'b2b_vip5', 'temporary_asia', 'temporary_global', 'deactivated'];
        if (!in_array($action, $valid_actions)) {
            wp_die('Invalid action specified.', 'Invalid Request', array('response' => 400));
        }

        // 각 사용자의 user_group을 업데이트
        foreach ($user_ids as $user_id) {
            // 현재 user_group 값을 조회
            $current_group = get_user_meta($user_id, 'user_group', true);
            // 현재 user_group 값을 user_group_old_value에 저장
            update_user_meta($user_id, 'user_group_old_value', $current_group);

            // user_group을 새로운 값으로 업데이트
            update_user_meta($user_id, 'user_group', sanitize_text_field($action));
            $current_datetime = current_time('mysql'); // 로컬 시간대 기준 현재 시간, KST
            update_user_meta($user_id, 'user_group_update_date', $current_datetime);

            // 'deactivated' 상태로 변경될 경우 deactivation_count 업데이트
            if ($action === 'deactivated') {
                $deactivation_count = get_user_meta($user_id, 'deactivation_count', true);
                $deactivation_count = is_numeric($deactivation_count) ? $deactivation_count + 1 : 1;
                update_user_meta($user_id, 'deactivation_count', $deactivation_count);
            }
        }

        // 현재 페이지 번호를 가져옵니다. 만약 설정되어 있지 않다면 기본값으로 1을 사용합니다.
        $current_paged = isset($_GET['paged']) ? $_GET['paged'] : 1;
        $country_filter = isset($_GET['country_filter']) ? $_GET['country_filter'] : '';
        $search_query = isset($_GET['s']) ? $_GET['s'] : '';
        $user_group_filter = isset($_GET['user_group_filter']) ? $_GET['user_group_filter'] : '';

        // 리다이렉트 URL에 현재 페이지 번호를 포함시킵니다.
        wp_redirect(
            add_query_arg(
                array(
                    'page' => 'must-user-management',
                    'changed' => count($user_ids),
                    'paged' => $current_paged,
                    'country_filter' => $country_filter,
                    's' => $search_query, // 검색어 추가
                    'user_group_filter' => $user_group_filter
                ),
                admin_url('admin.php')
            )
        );
        exit;

    }

    // 현재 페이지 번호 로딩과 정렬 매개변수 처리
    $paged = isset($_GET['paged']) ? max(1, intval($_GET['paged'])) : 1;
    $order = isset($_GET['order']) && in_array($_GET['order'], ['asc', 'desc']) ? $_GET['order'] : 'desc';
    $orderby = isset($_GET['orderby']) ? $_GET['orderby'] : 'user_registered';
    $countries = get_unique_countries_from_users();
    $country_filter = isset($_GET['country_filter']) ? $_GET['country_filter'] : '';
    $user_group_filter = isset($_GET['user_group_filter']) ? $_GET['user_group_filter'] : '';

    // 검색어 처리
    $search_query = isset($_GET['s']) ? sanitize_text_field($_GET['s']) : '';

    // 국가 이름을 코드로 변환하고 검색 쿼리에 적용
    $country_code = convert_country_name_to_code($search_query);
    if ($country_code) {
        $search_query = $country_code;
    }
    // 국가 필터가 설정된 경우 메타 쿼리에 국가 조건을 추가합니다.
    $meta_query = array('relation' => 'AND'); // 'AND' 관계를 기본으로 설정합니다.
    if (!empty($country_filter)) {
        $meta_query[] = array(
            'key' => 'billing_country',
            'value' => $country_filter,
            'compare' => '='
        );
    }
    if ($orderby === 'billing_country') {
        $meta_query[] = array(
            'key' => 'billing_country',
            'compare' => 'EXISTS'
        );
    }
    if (!empty($user_group_filter)) {
        $meta_query[] = array(
            'key' => 'user_group',
            'value' => $user_group_filter,
            'compare' => '='
        );
    }

    // 검색 조건 추가
    $search_meta_query = array('relation' => 'OR');
    if (!empty($search_query)) {
        $user_query_args['search'] = '*' . esc_attr($search_query) . '*';
        $user_query_args['search_columns'] = array('user_login', 'billing_company', 'user_email');

        $search_meta_query[] = array(
            'key' => 'billing_country',
            'value' => $search_query,
            'compare' => 'LIKE'
        );
        $search_meta_query[] = array(
            'key' => 'user_group',
            'value' => $search_query,
            'compare' => 'LIKE'
        );

        // 메타 쿼리를 통합
        $meta_query[] = $search_meta_query;
    }

    // WP_User_Query 인스턴스를 생성
    $user_query = new WP_User_Query(
        array(
            'role' => 'Customer',
            'orderby' => $orderby === 'billing_country' ? 'meta_value' : $orderby,
            'meta_key' => $orderby === 'billing_country' ? 'billing_country' : '',
            'order' => $order,
            'meta_query' => $meta_query,
            'number' => 25,
            'offset' => ($paged - 1) * 25
        )
    );


    // 전체 사용자 수 와 전체 페이지 수 계산
    $total_users = $user_query->get_total(); // 전체 사용자 수
    $total_pages = ceil($total_users / 25); // 전체 페이지 수


    // 페이지네이션 링크 출력
    $pagination_links = paginate_links(
        array(
            'base' => add_query_arg(array('paged' => '%#%', 'orderby' => $orderby, 'order' => $order, 'country_filter' => $country_filter), ''), // 현재 필터 및 정렬 설정 유지
            'format' => '?paged=%#%',
            'current' => $paged,
            'total' => $total_pages,
            'prev_text' => __('« Prev'),
            'next_text' => __('Next »'),
        )
    );

    ?>

        <div class="wrap">
            <h1>
                <?php echo esc_html(get_admin_page_title()); ?>
            </h1>
            <!-- 검색 폼 추가 -->
            <form id="must-user-management-search-form" method="get" action="">
                <input type="hidden" name="page" value="must-user-management">
                <div class="tablenav top">
                    <p class="search-box">
                        <label class="screen-reader-text" for="user-search-input">
                            <?php _e('Search Users:'); ?>
                        </label>
                        <input type="search" id="user-search-input" name="s" value="<?php echo esc_attr($search_query); ?>">
                        <input type="submit" id="search-submit" class="button" value="<?php _e('Search Users'); ?>">
                    </p>
                </div>
            </form>
            <!-- 유저그룹 필터 -->
            <form id="user-group-filter-form" method="get" action="<?php echo esc_url(admin_url('admin.php')); ?>">
                <input type="hidden" name="page" value="must-user-management" />
                <div class="alignleft actions">
                    <select name="user_group_filter" id="user-group-filter-selector">
                        <option value="">
                            <?php _e('모든 유저그룹'); ?>
                        </option>
                        <option value="pending" <?php selected($user_group_filter, 'pending'); ?>>
                            <?php _e('Pending'); ?>
                        </option>
                        <option value="disapproval" <?php selected($user_group_filter, 'disapproval'); ?>>
                            <?php _e('Disapproval'); ?>
                        </option>
                        <option value="b2b_global" <?php selected($user_group_filter, 'b2b_global'); ?>>
                            <?php _e('B2B Global'); ?>
                        </option>
                        <option value="b2b_asia" <?php selected($user_group_filter, 'b2b_asia'); ?>>
                            <?php _e('B2B Asia'); ?>
                        </option>
                        <option value="b2b_ru" <?php selected($user_group_filter, 'b2b_ru'); ?>>
                            <?php _e('B2B RU'); ?>
                        </option>
                        <option value="b2b_kz_kg" <?php selected($user_group_filter, 'b2b_kz_kg'); ?>>
                            <?php _e('B2B KZ_KG'); ?>
                        </option>
                        <option value="b2b_vip3" <?php selected($user_group_filter, 'b2b_vip3'); ?>>
                            <?php _e('B2B VIP3'); ?>
                        </option>
                        <option value="b2b_vip5" <?php selected($user_group_filter, 'b2b_vip5'); ?>>
                            <?php _e('B2B VIP5'); ?>
                        </option>
                        <option value="deactivated" <?php selected($user_group_filter, 'deactivated'); ?>>
                            <?php _e('Deactivated'); ?>
                        </option>
                        <option value="temporary_asia" <?php selected($user_group_filter, 'temporary_asia'); ?>>
                            <?php _e('Temporary Asia'); ?>
                        </option>
                        <option value="temporary_global" <?php selected($user_group_filter, 'temporary_global'); ?>>
                            <?php _e('Temporary Global'); ?>
                        </option>
                    </select>
                    <input type="submit" name="user_group_filter_action" id="user-group-filter-submit" class="button"
                        value="<?php _e('필터'); ?>">
                </div>
            </form>
            <!-- 국가 필터 -->
            <form id="country-filter-form" method="get" action="<?php echo esc_url(admin_url('admin.php')); ?>">
                <input type="hidden" name="page" value="must-user-management" />
                <div class="alignleft actions">
                    <select name="country_filter" id="country-filter-selector">
                        <option value="">
                            <?php _e('모든 국가'); ?>
                        </option>
                        <?php foreach ($countries as $country_code => $country_name): ?>
                                <option value="<?php echo esc_attr($country_code); ?>" <?php selected($country_filter, $country_code); ?>>
                                    <?php echo esc_html($country_name); // 나중에 국가 이름으로 바꿀 수 있습니다. ?>
                                </option>
                        <?php endforeach; ?>
                    </select>
                    <input type="submit" name="country_filter_action" id="country-filter-submit" class="button"
                        value="<?php _e('필터'); ?>">
                </div>
            </form>
            <!-- Bulk actions 드롭다운 -->
            <form id="must-user-management-bulk-form" method="post">
                <?php wp_nonce_field('bulk-user-group-change', 'must_user_management_nonce'); ?>
                <div class="alignleft actions bulkactions">
                    <label for="bulk-action-selector-top" class="screen-reader-text">
                        <?php _e('Select bulk action'); ?>
                    </label>
                    <select name="action" id="bulk-action-selector-top">
                        <option value="-1">
                            <?php _e('그룹 변경'); ?>
                        </option>
                        <option value="pending">
                            <?php _e('Change to Pending'); ?>
                        </option>
                        <option value="disapproval">
                            <?php _e('Change to Disapproval'); ?>
                        </option>
                        <option value="deactivated">
                            <?php _e('Change to Deactivated'); ?>
                        </option>
                        <option value="temporary_global">
                            <?php _e('Change to Temporary Global'); ?>
                        </option>
                        <option value="temporary_asia">
                            <?php _e('Change to Temporary Asia'); ?>
                        </option>
                        <option value="b2b_global">
                            <?php _e('Change to B2B_Global'); ?>
                        </option>
                        <option value="b2b_asia">
                            <?php _e('Change to B2B_Asia'); ?>
                        </option>
                        <option value="b2b_kz_kg">
                            <?php _e('Change to B2B_KZ_KG'); ?>
                        </option>
                        <option value="b2b_ru">
                            <?php _e('Change to B2B_RU'); ?>
                        </option>
                        <option value="b2b_vip3">
                            <?php _e('Change to B2B_VIP3'); ?>
                        </option>
                        <option value="b2b_vip5">
                            <?php _e('Change to B2B_VIP5'); ?>
                        </option>
                    </select>
                    <input type="submit" id="doaction" class="button action" value="<?php _e('적용'); ?>">
                </div>
                <!-- 상단 페이지네이션 출력 -->
                <?php if ($pagination_links): ?>
                        <div class="alignright actions">
                            <?php echo sprintf('<span class="displaying-num">%s items</span>', number_format_i18n($total_users)); ?>
                            <?php echo $pagination_links; ?>
                        </div>
                <?php endif; ?>
                <!-- 사용자 목록 테이블 -->
                <table class="wp-list-table widefat fixed striped users">
                    <thead>
                        <tr>
                            <th id="cb" class="manage-column column-cb check-column">
                                <label class="screen-reader-text" for="cb-select-all-1">
                                    <?php _e('Select All'); ?>
                                </label>
                                <input id="cb-select-all-1" type="checkbox">
                            </th>
                            <?php
                            // 기존 헤더 함수 호출
                            must_user_management_sortable_header('Username', 'user_login', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Company', 'billing_company', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Email', 'user_email', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Country', 'billing_country', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Registered Date', 'user_registered', $orderby, $order, $paged, $country_filter, $search_query);
                            // 'Last Order'와 'B2BKing Group'는 정렬 기능 없음
                            echo '<th>Last Order</th>';
                            echo '<th>MUST Group</th>';
                            echo '<th>Business License</th>';
                            must_user_management_sortable_header('유저그룹 갱신일', 'user_group_update_date', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('비활성 카운트', 'deactivation_count', $orderby, $order, $paged, $country_filter, $search_query);
                            ?>
                        </tr>
                    </thead>
                    <tbody id="the-list" data-wp-lists="list:user">
                        <?php
                        if (!empty($user_query->get_results())) {
                            foreach ($user_query->get_results() as $user) {
                                echo '<tr>';
                                echo '<th scope="row" class="check-column">';
                                echo '<input type="checkbox" name="users[]" id="user_' . esc_attr($user->ID) . '" class="administrator" value="' . esc_attr($user->ID) . '">';
                                echo '<label class="screen-reader-text" for="user_' . esc_attr($user->ID) . '">' . sprintf(__('Select %s'), $user->user_login) . '</label>';
                                echo '</th>';
                                echo '<td><a href="' . esc_url(get_edit_user_link($user->ID)) . '">' . esc_html($user->user_login) . '</a></td>';
                                echo '<td>' . esc_html($user->billing_company) . '</td>';
                                echo '<td>' . esc_html($user->user_email) . '</td>';
                                echo '<td>' . esc_html(get_country_name_by_code($user->billing_country)) . '</td>';
                                $months_since_registration = get_months_since_registration($user->user_registered);
                                echo '<td>' . esc_html(date("Y-m-d", strtotime($user->user_registered))) . ' / ' . $months_since_registration . '개월' . '</td>';
                                echo '<td>' . get_last_order_date_for_user($user->ID) . '</td>';
                                echo '<td>' . esc_html($user->user_group) . '</td>';
                                // 비즈니스 라이선스 파일 링크 표시
                                $business_license_url = get_user_meta($user->ID, 'b2b_business_license_file', true);
                                if (!empty($business_license_url)) {
                                    echo '<td><a href="' . esc_url($business_license_url) . '" target="_blank">View</a></td>';
                                } else {
                                    echo '<td>X</td>'; // 파일 링크가 없는 경우
                                }
                                echo '<td>' . esc_html($user->user_group_update_date) . '</td>';
                                echo '<td>' . esc_html($user->deactivation_count) . '</td>';

                                echo '</tr>';
                            }
                        } else {
                            echo '<tr><td colspan="8">No customers found.</td></tr>';
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th id="cb" class="manage-column column-cb check-column">
                                <label class="screen-reader-text" for="cb-select-all-2">
                                    <?php _e('Select All'); ?>
                                </label>
                                <input id="cb-select-all-2" type="checkbox">
                            </th>
                            <?php
                            // 기존 헤더 함수 호출
                            must_user_management_sortable_header('Username', 'user_login', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Company', 'billing_company', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Email', 'user_email', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Country', 'billing_country', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('Registered Date', 'user_registered', $orderby, $order, $paged, $country_filter, $search_query);
                            // 'Last Order'와 'B2BKing Group'는 정렬 기능 없음
                            echo '<th>Last Order</th>';
                            echo '<th>MUST Group</th>';
                            echo '<th>Business License</th>';
                            must_user_management_sortable_header('유저그룹 갱신일', 'user_group_update_date', $orderby, $order, $paged, $country_filter, $search_query);
                            must_user_management_sortable_header('비활성 카운트', 'deactivation_count', $orderby, $order, $paged, $country_filter, $search_query);
                            ?>
                        </tr>
                    </tfoot>
                </table>
            </form>

            <?php

            // 하단 페이지네이션 출력
            if ($pagination_links) {
                echo '<div class="tablenav"><div class="tablenav-pages">';
                echo sprintf('<span class="displaying-num">%s items</span>', number_format_i18n($total_users));
                echo $pagination_links;
                echo '</div></div>';
            }
            ?>

            <?php
}

add_action('admin_menu', 'must_user_management_menu');