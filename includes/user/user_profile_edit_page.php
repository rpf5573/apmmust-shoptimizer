<?php
// apM MUST 유저 관리 2024.01.31 베타테스트
// 유저 프로파일 편집 페이지에 커스텀 필드 
function add_user_group_dropdown_and_custom_fields($user)
{
  // 사용자 그룹 메타 데이터 가져오기
  $user_group = get_the_author_meta('user_group', $user->ID);

  // 사용자 그룹 드롭다운
  ?>
  <h3>apM MUST B2B 유저 정보</h3>
    <table class="form-table">
        <tr>
            <th><label for="additional_whatsapp"><?php _e("WhatsApp", "woocommerce"); ?></label></th>
            <td>
                <input type="text" name="additional_whatsapp" id="additional_whatsapp" value="<?php echo esc_attr(get_user_meta($user->ID, 'additional_whatsapp', true)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th><label for="billing_vat"><?php _e("VAT No.", "woocommerce"); ?></label></th>
            <td>
                <input type="text" name="billing_vat" id="billing_vat" value="<?php echo esc_attr(get_user_meta($user->ID, 'billing_vat', true)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th><label for="billing_eori"><?php _e("EORI No.", "woocommerce"); ?></label></th>
            <td>
                <input type="text" name="billing_eori" id="billing_eori" value="<?php echo esc_attr(get_user_meta($user->ID, 'billing_eori', true)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th><label for="additional_shop_url"><?php _e("Shop URL", "woocommerce"); ?></label></th>
            <td>
                <input type="url" name="additional_shop_url" id="additional_shop_url" value="<?php echo esc_attr(get_user_meta($user->ID, 'additional_shop_url', true)); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <?php 
        $business_license_url = get_user_meta($user->ID, 'b2b_business_license_file', true);
        if (!empty($business_license_url)) {
            ?>
            <tr>
                <th>
                    <label for="b2b_business_license_file"><?php _e("Business License", "woocommerce"); ?></label>
                </th>
                <td>
                    <a href="<?php echo esc_url($business_license_url); ?>" target="_blank"><?php _e("View Uploaded File", "woocommerce"); ?></a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
}

// 사용자 프로필 및 편집 페이지에 훅 추가
add_action('show_user_profile', 'add_user_group_dropdown_and_custom_fields');
add_action('edit_user_profile', 'add_user_group_dropdown_and_custom_fields');

// Add custom fields to the edit account form
function custom_woocommerce_edit_account_form()
{
  $user_id = get_current_user_id();
  $user = get_userdata($user_id);

  if (!$user)
    return;

  $whatsapp = get_user_meta($user_id, 'additional_whatsapp', true);
  $vat_number = get_user_meta($user_id, 'billing_vat', true);
  $eori_number = get_user_meta($user_id, 'billing_eori', true);
  $shop_url = get_user_meta($user_id, 'additional_shop_url', true);

  ?>
  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="additional_whatsapp">
      <?php _e('WhatsApp', 'woocommerce'); ?>
    </label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="additional_whatsapp"
      id="additional_whatsapp" value="<?php echo esc_attr($whatsapp); ?>">
  </p>
  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="billing_vat">
      <?php _e('VAT No.', 'woocommerce'); ?>
    </label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_vat" id="billing_vat"
      value="<?php echo esc_attr($vat_number); ?>">
  </p>
  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="billing_eori">
      <?php _e('EORI No.', 'woocommerce'); ?>
    </label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_eori" id="billing_eori"
      value="<?php echo esc_attr($eori_number); ?>">
  </p>
  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label for="additional_shop_url">
      <?php _e('Shop URL', 'woocommerce'); ?>
    </label>
    <input type="url" class="woocommerce-Input woocommerce-Input--url input-text" name="additional_shop_url"
      id="additional_shop_url" value="<?php echo esc_attr($shop_url); ?>">
  </p>
  <?php
}

add_action('woocommerce_edit_account_form', 'custom_woocommerce_edit_account_form');

// Save Custom Fields from Admin User Profile
function custom_save_admin_user_profile_fields($user_id)
{
  if (!current_user_can('edit_user', $user_id)) {
    return false;
  }

  update_user_meta($user_id, 'additional_whatsapp', $_POST['additional_whatsapp']);
  update_user_meta($user_id, 'billing_vat', $_POST['billing_vat']);
  update_user_meta($user_id, 'billing_eori', $_POST['billing_eori']);
  update_user_meta($user_id, 'additional_shop_url', $_POST['additional_shop_url']);
}

add_action('personal_options_update', 'custom_save_admin_user_profile_fields');
add_action('edit_user_profile_update', 'custom_save_admin_user_profile_fields');


// Save custom fields from account form
function custom_save_woocommerce_edit_account($customer_id)
{
  if (isset($_POST['additional_whatsapp'])) {
    update_user_meta($customer_id, 'additional_whatsapp', sanitize_text_field($_POST['additional_whatsapp']));
  }

  if (isset($_POST['billing_vat'])) {
    update_user_meta($customer_id, 'billing_vat', sanitize_text_field($_POST['billing_vat']));
  }

  if (isset($_POST['billing_eori'])) {
    update_user_meta($customer_id, 'billing_eori', sanitize_text_field($_POST['billing_eori']));
  }

  if (isset($_POST['additional_shop_url'])) {
    update_user_meta($customer_id, 'additional_shop_url', esc_url_raw($_POST['additional_shop_url']));
  }
}

add_action('woocommerce_save_account_details', 'custom_save_woocommerce_edit_account');