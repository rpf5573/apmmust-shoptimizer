<?php

add_shortcode("desktop_nav_menu", function () {
  ob_start();

  $menu_items = apmmust_get_primary_menu_item_list_with_children();
  $non_brand_menu_item_list = array_filter($menu_items, function ($menu_item) {
    return !in_array(strtolower($menu_item["name"]), ["brand"]);
  });
  $brand_menu_item = current(
    array_filter($menu_items, function ($menu_item) {
      return in_array(strtolower($menu_item["name"]), ["brand"]);
    })
  );

  $featured_brand_list = apmmust_get_featured_brand_list();
  $new_brand_term_list = apmmust_get_new_brand_term_list();

  $buildings = apmmust_get_buildings();
  function_exists('ray') && ray('building', $buildings);
  ?>

  <nav
    id="site-navigation"
    class="main-navigation hover-intent"
    aria-label="Primary Navigation"
  >
    <div class="primary-navigation">
      <div class="menu-primary-menu-container">
        <ul id="menu-primary-mega-menu" class="menu">
          <!-- None Brands -->
          <?php foreach ($non_brand_menu_item_list as $menu_item) {
            $product_count_today = apmmust_get_product_count_by_slug(
              strtolower($menu_item["name"]),
              1
            );
            $product_count_seven_days = apmmust_get_product_count_by_slug(
              strtolower($menu_item["name"]),
              7
            );
          ?>
            <li class="full-width col-2 dropdown-open menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children">
              <a href="<?php echo $menu_item["href"]; ?>" class="cg-menu-link main-menu-link"><span><?php echo $menu_item["name"]; ?></span></a>
              <span class="caret"></span>
              <div class="sub-menu-wrapper main">
                <ul class="sub-menu">
                  <div class="hr-vertical"></div>
                  <div class="new-arrival mobile-hidden">
                    <div class="new-arrival-cont today">
                      <p>NEW IN <b>TODAY</b></p>
                      <span class="count"><?php echo $product_count_today; ?></span>
                    </div>
                    <div class="new-arrival-cont last">
                      <p>NEW THIS <b>WEEK</b></p>
                      <span class="count"><?php echo $product_count_seven_days; ?></span>
                    </div>
                    <div class="new-arrival-cont new-arrival-btn">
                      <a href="<?php echo $menu_item['href']; ?>">CHECK NEW IN</a>
                    </div>
                  </div>
                  <div class="hr-vertical"></div>
                  <?php
                    $children = $menu_item["children"];
                    foreach ($children as $child) {
                      if (!$child["description"]) {
                  ?>
                        <li id="nav-menu-item-10567018" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children">
                          <a href="<?php echo $child["href"]; ?>" class="cg-menu-link sub-menu-link"><span><b><?php echo $child["name"]; ?></b><span class="sub"> </span></span></a><span class="caret"></span>
                          <div class="sub-menu-wrapper sub">
                            <ul class="sub-menu">
                              <div class="hr-vertical"></div>
                              <?php
                                $children_children = $child["children"];
                                foreach ($children_children as $child_child) {
                              ?>
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat">
                                  <a href="<?php echo $child_child["href"]; ?>" class="cg-menu-link sub-menu-link"><span><?php echo $child_child["name"]; ?></span></a>
                                </li>
                              <?php
                                }
                              ?>
                            </ul>
                          </div>
                        </li>
                      <?php
                      } else {
                      ?>
                        <li class="menu-item-image menu-item menu-item-type-custom menu-item-object-custom">
                          <a href="<?php echo $child["href"]; ?>" class="cg-menu-link sub-menu-link">
                            <?php echo $child["description"]; ?>
                          </a>
                        </li>
                      <?php
                      }
                      ?>
                    <div class="hr-vertical"></div>
                  <?php
                    }
                  ?>
                </ul>
              </div>
            </li>
          <?php
          } ?>

          <!-- Brands -->
          <li
            id="nav-menu-item-3234929"
            class="full-width dropdown-open menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"
          >
            <a href="/brand-list/" class="cg-menu-link main-menu-link"><span>Brands</span></a
            ><span class="caret"></span>
            <div class="sub-menu-wrapper main">
              <ul class="sub-menu">
                <div class="hr-vertical"></div>
                <li class="col-1 dropdown-open menu-item menu-item-type-taxonomy menu-item-object-product_brand menu-item-has-children">
                  <a href="#" class="cg-menu-link sub-menu-link"><span><b>Malls</b><span class="sub"> </span></span></a><span class="caret"></span>
                  <div class="sub-menu-wrapper sub">
                    <ul class="sub-menu"> <?php
                      foreach($buildings as $building) {
                        if (strtolower($building['name']) === 'other') {
                          continue;
                        }
                        ?>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-product_brand">
                          <a href="<?php echo $building['href']; ?>" class="cg-menu-link sub-menu-link">
                            <span><?php echo $building['name']; ?></span>
                          </a>
                        </li> <?php
                      } ?>
                      <li class="menu-item menu-item-type-taxonomy menu-item-object-product_brand">
                        <a href="<?php echo home_url( 'brand-list' );?>" class="cg-menu-link sub-menu-link">
                          <span><b>See More</b></span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
                <div class="hr-vertical"></div>
                <li
                  class="col-1 dropdown-open menu-item menu-item-type-taxonomy menu-item-object-product_brand menu-item-has-children"
                >
                  <a
                    href="#"
                    class="cg-menu-link sub-menu-link"
                    ><span><b>NEW BRANDS</b><span class="sub"> </span></span></a
                  ><span class="caret"></span>
                  <div class="sub-menu-wrapper sub">
                    <ul class="sub-menu">
                      <div class="hr-vertical"></div>
                      <?php
                      foreach($new_brand_term_list as $term_item) { ?>
                        <li
                          class="menu-item menu-item-type-taxonomy menu-item-object-product_brand"
                        >
                          <a
                            href="<?php echo $term_item['term_link']; ?>"
                            class="cg-menu-link sub-menu-link"
                            ><span><?php echo $term_item['name']; ?></span></a
                          >
                        </li> <?php
                      } ?>
                    </ul>
                  </div>
                </li>
                <div class="hr-vertical"></div>
                <?php
                if (!empty($featured_brand_list)) { ?>
                <div class="featured-wrap">
                  <h3>Featured Brands</h3>
                  <div class="featured-cont">
                    <?php
                      foreach($featured_brand_list as $brand_item) { ?>
                        <div class="featured-item">
                          <div class="featured-imgbox">
                            <a href="<?php echo $brand_item['term_link']; ?>" style="background-image: url('<?php echo $brand_item['img_url']; ?>')"></a>
                          </div>
                          <div class="featured-desc">
                            <div class="desc-left">
                              <a href="<?php echo $brand_item['term_link']; ?>"><?php echo $brand_item['name']; ?></a>
                            </div>
                            <div class="desc-right">
                              <a href="<?php echo $brand_item['term_link']; ?>">Show Now</a>
                            </div>
                          </div>
                        </div> <?php
                      }
                    ?>
                  </div>
                </div> <?php
                } ?>
                <div class="hr-vertical"></div>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <?php return ob_get_clean();
});
