<?php
// 상품 상세페이지 > 탭을 우측 아코디언으로 이동
add_action( 'woocommerce_single_product_summary', function() { ?>
<style>
  // 하단의 탭은 가려준다
  .product-details-wrapper + .woocommerce-tabs.wc-tabs-wrapper {
    display: none !important;
  }
  .accordion-content > h2 {
    display: none !important;
  }
</style>

<?php
  wc_get_template( 'single-product/tabs/toggles.php' );
}, 55 ); // 55가 중요합니다

// 상품 상세 아코디언에 무게 계산기를 넣습니다
add_filter( 'woocommerce_product_tabs', 'apmmust_add_weight_calculator_to_the_product_tab' );
function apmmust_add_weight_calculator_to_the_product_tab( $tabs ) {

    $tabs['apmmust_weight_calculator_tab'] = array(
        'title'     => __( 'Weight Calculator', 'text-domain' ),
        'priority'  => 50,
        'callback'  => 'apmmust_weight_calculator_callback'
    );

    return $tabs;
}
function apmmust_weight_calculator_callback() {
    echo do_shortcode( '[apmmust_weight_calculator]' );
}

// 교환 및 환불에 관한 탭과 추가 탭
function custom_product_tabs( $tabs ) {
    // 기존 탭
    $tabs['return_refund_tab'] = array(
        'title'    => __( 'Return, Refund & Credit Note', 'add_product_tab' ),
        'priority' => 1,
        'callback' => 'return_refund_tab_content'
    );

    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'custom_product_tabs' );

// 교환 및 환불 탭의 내용
function return_refund_tab_content() {
    ?>
    <h4>Return & Exchange</h4>
    <ol>
        <li>Returns are only possible <strong>within one week of receiving the product</strong> and only if there is an issue with the product.</li>
        <li>If the Inspection Option is not applied, the client must bear the return shipping costs.</li>
    </ol>

    <h4>Credit Note</h4>
    <ol>
        <li>Partial payment cancellation is only available for payments made through PayPal.</li>
        <li>For payments made through Credit Cards, partial payment cancellation is not possible.</li>
        <li>In cases of partial order cancellation for orders made through Credit Card/Swift, the refund will be issued as a Credit Note to the client. This Credit Note can be used for future orders and shipping costs.</li>
        <li>The validity of the Credit Note is <strong>lifetime</strong>, unlike other intermediary agents and wholesale brands where it is automatically deleted after 3 months.</li>
    </ol>

    <h4>Refund</h4>
    <ol>
		<li>Surcharge fees for payment methods are not refundable.</li>
        <li>Refunds are not available if the order has been processed and more than two weeks have passed, except in cases where the products are faulty upon delivery or if the wrong items were shipped.</li>
        <li>If a parcel cannot be delivered due to logistical reasons or incorrect address provided by customer, only amount listed on parcel is refundable.</li>
    </ol>
    <?php
}

// 상품 상세 아코디언에 상품의 소재 내용을 넣습니다
add_filter( 'woocommerce_product_tabs', 'apmmust_add_composition_table_to_the_product_tab' );
function apmmust_add_composition_table_to_the_product_tab( $tabs ) {

    $tabs['apmmust_composition_table_tab'] = array(
        'title'     => 'Composition',
        'priority'  => 10,
        'callback'  => 'apmmust_composition_table_callback'
    );

    return $tabs;
}
// 상품 상세 페이지에 소재 내용을 표시하는 콜백 함수
function apmmust_composition_table_callback() {
    global $product;

    // 상품의 'pa_composition' 속성 값과 관련된 메타 데이터를 가져옵니다.
    $composition_list = rwmb_get_value('composition_group_id', [], $product->get_id());

    // 소재 내용이 있는 경우, 테이블 형태로 출력
    if (!empty($composition_list)) {
        echo '<table class="apmmust-composition-table">';
        echo '<thead><tr><th>Material</th><th>Composition</th></tr></thead>';
        echo '<tbody>';
        foreach ($composition_list as $composition) {
            // 소재의 이름을 용어 ID를 통해 가져옵니다.
            $term = get_term_by('id', $composition['composition_fabric'], 'pa_composition');
            $material_name = $term ? $term->name : 'Unknown'; // 용어 조회 실패 시 'Unknown'
            $composition_rate = $composition['composition_fabric_rate']; // 소재 비율

            echo '<tr>';
            echo '<td>' . esc_html($material_name) . '</td>'; // 소재 이름
            echo '<td>' . esc_html($composition_rate) . '%</td>'; // 소재 비율
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
    } else {
        echo '<p>Inqury</p>';
    }
}
