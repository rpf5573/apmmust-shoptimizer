<?php

class Custom_Hello_Widget extends WP_Widget {
    public function __construct() {
        parent::__construct(
            'notice_marquee_widget',
            __('Notice Marquee Widget', 'apmmust'),
            array( 'description' => __('탑바에 나오는 공지사항 marquee', 'apmmust') )
        );
    }

    public function widget( $args, $instance ) {
        $notice_list = $this->get_notice_list();
        $html = $this->render_html($args, $notice_list);
        echo $html;
    }

    private function get_notice_list() {
        return rwmb_meta( 'top-bar-notice-field', [ 'object_type' => 'setting' ], 'top-bar-notice' ) ?: [];
    }

    private function render_html($args, $notice_list) {
        ob_start();

        // if ( !is_user_logged_in() ) {
        // check admin
        if ( current_user_can('administrator') ) { ?>
            <style>
                body {
                    padding-top: 32px !important;
                }
            </style> <?php
        }

        ?>
        <style>
            .marquee-widget .marquee {
                border: 2px solid black;
                overflow: hidden;
                white-space: nowrap;
                width: 620px;
                overflow: hidden;
            }

            .marquee-widget .marquee .inner {
                display: flex;
                justify-content: flex-start;
                position: relative;
            }

            .marquee-widget .marquee .inner > div {
                display: flex;
                column-gap: 20px;
                animation: marquee 28s linear infinite;
                padding-left: 10px;
                padding-right: 10px;
            }

            .marquee-widget .marquee .inner > div:nth-child(2) {
                display: flex;
                column-gap: 20px;
                animation: marquee2 28s linear infinite;
                animation-delay: 14s;
            }

            @keyframes marquee {
                from {
                    transform: translateX(100%);
                }
                to {
                    transform: translateX(-100%);
                }
            }

            @keyframes marquee2 {
                from {
                    transform: translateX(0%);
                }
                to {
                    transform: translateX(-200%);
                }
            }

            @media (max-width: 768.5px) {
                .marquee-widget .marquee {
                    width: 240px;
                }
                .marquee-widget .marquee .inner {
                    display: flex;
                    justify-content: flex-start;
                    position: relative;
                    left: -100%;
                }
                .top-bar>.col-full .top-bar-right {
                    justify-content: center;
                }
            }

        </style>
        <div class="marquee-widget">
            <?php echo $args['before_widget']; ?>
            <div class="marquee">
                <div class="inner">
                    <div>
                        <?php foreach($notice_list as $notice): ?>
                            <span><?php echo esc_html($notice); ?></span>
                        <?php endforeach; ?>
                    </div>
                    <div>
                        <?php foreach($notice_list as $notice): ?>
                            <span><?php echo esc_html($notice); ?></span>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php echo $args['after_widget']; ?>
        </div>
        <?php
        return ob_get_clean();
    }
}

function register_custom_hello_widget() {
    register_widget( 'Custom_Hello_Widget' );
}
add_action( 'widgets_init', 'register_custom_hello_widget' );