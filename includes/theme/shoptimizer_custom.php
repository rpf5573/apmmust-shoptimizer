<?php
// 상품 클릭할때 새탭에서 열리도록 (Infinite Scroll을 위해서)
// 2023-10-06 - kmong 윤매니저
// 이미지
//add_action( 'init', function() {
//  remove_action( 'woocommerce_before_shop_loop_item_title', 'shoptimizer_template_loop_image_link_open', 5 );
//});
//add_action( 'woocommerce_before_shop_loop_item_title', function() {
//  echo '<a href="' . get_the_permalink() . '" aria-label="' . get_the_title() . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link" target="_blank">';
//}, 5);

// 카트페이지에 세일 가격 표시하기
add_filter( 'woocommerce_cart_item_price', 'cg_cart_table_price_display', 30, 3 );
  
function cg_cart_table_price_display( $price, $values, $cart_item_key ) {
   $slashed_price = $values['data']->get_price_html();
   $is_on_sale = $values['data']->is_on_sale();
   if ( $is_on_sale ) {
      $price = $slashed_price;
   }
   return $price;
}

// SKU, 카테고리, 태그 위치 싱글페이지 안에서 
add_action( 'wp', 'shoptimizer_reorder_product_meta', 99 );
function shoptimizer_reorder_product_meta() {
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_single_meta', 15 );
}
add_action( 'woocommerce_product_meta_start', 'add_brand_info_to_product_meta' );

add_action( 'woocommerce_product_meta_start', 'add_brand_info_to_product_meta' );

//상품 상세 페이지에 Brand meta
function add_brand_info_to_product_meta() {
    global $product;

    // Get the terms for the 'product_brand' taxonomy
    $brands = wp_get_post_terms( $product->get_id(), 'product_brand' );

    if ( ! empty( $brands ) && ! is_wp_error( $brands ) ) {
        // Just get the first brand
        $brand = $brands[0];

        // Get the archive link for the brand taxonomy term
        $brand_link = get_term_link( $brand );

        // Check if the link is a valid URL
        if ( ! is_wp_error( $brand_link ) ) {
            // Output the brand with a link to the brand's archive page
            echo '<span class="product-brand">Brand: <a href="' . esc_url( $brand_link ) . '">' . esc_html( $brand->name ) . '</a></span><br />';
        }
    }
}

// 초기화 시점에 상품명 출력 커스터마이징을 위한 액션을 추가합니다.
add_action('init', 'customize_shop_loop_item_title');
function customize_shop_loop_item_title() {
    // WooCommerce의 기본 상품명 출력 액션을 제거하여, 사용자 정의 방식으로 대체할 수 있도록 합니다.
    remove_action('woocommerce_shop_loop_item_title', 'shoptimizer_loop_product_title', 10);
}

// 상품명과 함께 사용자 정의 정보(카테고리 및 브랜드)를 출력하기 위한 액션을 추가합니다.
add_action('woocommerce_shop_loop_item_title', 'add_custom_product_info', 10);
function add_custom_product_info() {
    global $post; // 글로벌 $post 객체를 사용하여 현재 루프 중인 상품의 정보에 접근합니다.

    $post_id = $post->ID; // 현재 상품의 ID를 가져옵니다.
    $cache_key = 'custom_product_info_' . $post_id; // 상품별 캐시 키를 생성합니다.
    $custom_product_info = get_transient($cache_key); // 캐시된 상품 정보를 조회합니다.

    // 캐시된 정보가 없다면 새로운 정보를 생성하고 캐시합니다.
    if (!$custom_product_info) {
        ob_start(); // 출력 버퍼링을 시작합니다.

        // 테마 설정에서 카테고리 정보를 표시할지 여부를 결정하는 옵션을 조회합니다.
        $display_category = shoptimizer_get_option('shoptimizer_layout_woocommerce_display_category');
        if ($display_category === true) {
            // 상품의 카테고리 목록을 출력합니다.
            $category_list = wc_get_product_category_list($post_id, ', ', '', '');
            if ($category_list) {
                echo '<p class="product__categories">' . $category_list . '</p>';
            }

            // 상품의 브랜드 목록을 출력합니다.
            $brand_list = get_the_term_list($post_id, 'product_brand', '', ',', '');
            if ($brand_list) {
                echo '<p class="product__categories product__brands">' . $brand_list . '</p>';
            }
        }

        // 상품명을 링크와 함께 출력합니다. 링크는 새 탭에서 열립니다.
        echo '<div class="woocommerce-loop-product__title"><a href="' . get_the_permalink($post_id) . '" aria-label="' . get_the_title($post_id) . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link" target="_blank">' . get_the_title($post_id) . '</a></div>';

        $custom_product_info = ob_get_clean(); // 버퍼링된 내용을 가져와서 변수에 저장하고 버퍼를 종료합니다.
        set_transient($cache_key, $custom_product_info, HOUR_IN_SECONDS); // 생성된 정보를 캐시에 저장합니다. 캐시는 1시간 동안 유효합니다.
    }

    // 캐시된 또는 새로 생성된 상품 정보를 출력합니다.
    echo $custom_product_info;
}

// Thankspage의 상품 리스트 썸네일 표시
add_action( 'woocommerce_order_details_after_order_table_items', 'display_thumbnails_after_order_table_items', 10, 1 );
function display_thumbnails_after_order_table_items( $order ) {
    // 주문에 포함된 상품들을 반복 처리합니다.
    foreach ( $order->get_items() as $item_id => $item ) {
        // 상품 객체를 가져옵니다.
        $product = $item->get_product();

        // 상품이 있으면 썸네일을 출력합니다.
        if ( $product ) {
            echo '<div style="margin-bottom: 10px;">';
            echo $product->get_image( array( 50, 50 ) );  // 100px x 100px 크기의 상품 썸네일
            echo ' ' . $item->get_name();
            echo '</div>';
        }
    }
}

// 사용자 정보를 대쉬보드에 표시
add_action('woocommerce_account_dashboard', 'display_user_group_dashboard');
function display_user_group_dashboard() {
    $user_id = get_current_user_id();
    $user_group = get_user_meta($user_id, 'user_group', true);
    $user_group_update_date = get_user_meta($user_id, 'user_group_update_date', true);

    // 사용자 그룹별 정보를 배열로 관리
    $user_groups_info = [
        'pending' => ['display_text' => 'Pending', 'discount_percentage' => 0, 'moq' => 'N/A'],
        'deactivated' => ['display_text' => 'Deactivated', 'discount_percentage' => 0, 'moq' => 'N/A'],
        'disapproval' => ['display_text' => 'Disapproval', 'discount_percentage' => 0, 'moq' => 'N/A'],
        'temporary_global' => ['display_text' => 'Temporary Activation for a month', 'discount_percentage' => 0, 'moq' => 10],
        'temporary_asia' => ['display_text' => 'Temporary Activation for a month', 'discount_percentage' => 0, 'moq' => 10],
        'b2b_global' => ['display_text' => 'B2B Global', 'discount_percentage' => 0, 'moq' => 5],
        'b2b_asia' => ['display_text' => 'B2B Asia', 'discount_percentage' => 0, 'moq' => 10],
        'b2b_kz_kg' => ['display_text' => 'B2B KZ, KG', 'discount_percentage' => 0, 'moq' => 10],
        'b2b_vip3' => ['display_text' => 'B2B VIP3', 'discount_percentage' => 3, 'moq' => 3],
        'b2b_vip5' => ['display_text' => 'B2B VIP5', 'discount_percentage' => 5, 'moq' => 3],
    ];

    // 사용자 그룹 정보 설정
    $group_info = isset($user_groups_info[$user_group]) ? $user_groups_info[$user_group] : [
        'display_text' => esc_html($user_group), 
        'discount_percentage' => "N/A", 
        'moq' => 5
    ];

    // 모바일 친화적인 형태로 데이터 표시
    if (!empty($user_group)) {
        echo '<div class="user-group-dashboard">';
        echo '<h4>Your currently status:</h4>';
        echo '<div class="user-group-item"><strong>User Group:</strong> ' . $group_info['display_text'] . '</div>';
        echo '<div class="user-group-item"><strong>User Group Date:</strong> ' . esc_html($user_group_update_date) . ' KST</div>';
        echo '<div class="user-group-item"><strong>SWIFT Discount Rate:</strong> ' . $group_info['discount_percentage'] . '%</div>';
        echo '<div class="user-group-item"><strong>Apply volume discount for the same product MOQ:</strong> ' . $group_info['moq'] . 'pcs</div>';
        echo '</div>';
    }
}

// 'Billing Company' 컬럼을 사용자 목록에 추가
function add_woocommerce_billing_company_column($column) {
    $column['billing_company'] = 'Billing Company';
    return $column;
}
add_filter('manage_users_columns', 'add_woocommerce_billing_company_column');

// 'Billing Company' 컬럼에 내용 채우기
function add_woocommerce_billing_company_column_content($value, $column_name, $user_id) {
    if ('billing_company' == $column_name) {
        // WooCommerce의 'billing_company' 사용자 메타 데이터에서 값을 가져옵니다.
        $user_billing_company = get_user_meta($user_id, 'billing_company', true);
        return $user_billing_company;
    }
    return $value;
}
add_action('manage_users_custom_column', 'add_woocommerce_billing_company_column_content', 10, 3);
