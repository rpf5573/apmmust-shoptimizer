<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_User_Group_Deactivated_Email extends WC_Email
{

    public function __construct()
    {
        // Email slug - 이메일 식별자
        $this->id = 'wc_user_group_deactivated';
        // 이메일 제목
        $this->title = __('User Group Deactivated', 'text-domain');
        // 관리자 설정에서 보이는 설명
        $this->description = __('This email is sent to a user when their user group is deactivated.', 'text-domain');
        // HTML 및 Plain 텍스트 이메일 템플릿 경로 지정
        $this->template_html = 'user-group-deactivated.php';
        $this->template_plain = 'plain/user-group-deactivated.php';
        // 이메일이 고객에게 보내지는 것인지 표시
        $this->customer_email = true;
        // 템플릿 기본 경로 지정
        $this->template_base = '/var/www/html/wp-content/themes/shoptimizer-child-theme/includes/emails/template/';


        // 이메일 트리거 - 사용자 그룹 비활성화 알림을 트리거하는 액션 후크
        add_action('user_group_deactivated_notification', array($this, 'trigger'), 10, 1);

        // 부모 클래스의 생성자 호출
        parent::__construct();

        // 이메일 제목 및 헤더 설정 (옵션)
        $this->subject = __('Your user group has been deactivated.', 'text-domain');
        $this->heading = __('User Group Deactivated', 'text-domain');
    }

    public function trigger($user_id)
    {
        $this->object = new WP_User($user_id);

        if ($this->object->user_email) {
            $this->recipient = $this->object->user_email;
            // 이메일 발송
            $this->send($this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments());
        }
    }

    public function get_content_html()
    {
        ob_start();
        wc_get_template($this->template_html, array(
            'email_heading' => $this->get_heading(),
            'user' => $this->object,
            'email' => $this,
        ), '', $this->template_base);
        return ob_get_clean();
    }

    public function get_content_plain()
    {
        ob_start();
        wc_get_template($this->template_plain, array(
            'email_heading' => $this->get_heading(),
            'user' => $this->object,
            'email' => $this,
        ), '', $this->template_base);
        return ob_get_clean();
    }

    public function get_subject()
    {
        return $this->format_string($this->get_option('subject', 'Your account has been deactivated.'));
    }

    public function get_heading()
    {
        return $this->format_string($this->get_option('heading', 'Deactivated'));
    }
}
