<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class WC_User_Group_Temporary_Email extends WC_Email {

    public function __construct() {
        $this->id    = 'wc_user_group_temporary';
        $this->title = 'User Group Temporary Approval';
        $this->description = 'This email is sent to a user when their user group is temporarily approved.';
        $this->template_html  = '/user-group-temporary.php';
        $this->template_plain = '/plain/user-group-temporary.php';
        $this->customer_email = true;
        $this->template_base = '/var/www/html/wp-content/themes/shoptimizer-child-theme/includes/emails/template';

        add_action( 'user_group_temporary_notification', array( $this, 'trigger' ), 10, 2 );

        parent::__construct();
    }

    public function trigger( $user_id, $group ) {
        $this->object = new WP_User( $user_id );
        $this->user_group = $group;

        if ( $this->object->user_email ) {
            $this->recipient = $this->object->user_email;
            $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
        }
    }

    public function get_content_html() {
        ob_start();
        wc_get_template( $this->template_html, array(
            'email_heading' => $this->get_heading(),
            'user_group'    => $this->user_group,
            'email'         => $this,
        ), '', $this->template_base );
        return ob_get_clean();
    }

    public function get_content_plain() {
        ob_start();
        wc_get_template( $this->template_plain, array(
            'email_heading' => $this->get_heading(),
            'user_group'    => $this->user_group,
            'email'         => $this,
        ), '', $this->template_base );
        return ob_get_clean();
    }

    public function get_subject() {
        return $this->format_string( $this->get_option( 'subject', 'Your account has been temporarily re-activated!' ) );
    }

    public function get_heading() {
        return $this->format_string( $this->get_option( 'heading', 'Temporarily Re-activated' ) );
    }
}
