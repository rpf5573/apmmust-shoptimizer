<?php
/**
 * User Group Disapproved Email Template.
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php echo sprintf( __("Dear %s,", 'woocommerce'), $user->display_name ); ?></p>
<p><?php _e( ' We regret to inform you that your account application has been declined following our review process. Based on the information provided, such as your business license and shop URL, we were unable to confirm your status as a retailer.', 'woocommerce' ); ?></p>
<p><?php _e( 'This decision was made with careful consideration, and we understand the inconvenience this may cause.', 'woocommerce' ); ?></p>

<p><?php _e( 'However, should you be able to provide documentation that clearly verifies your status as a retailer, we welcome you to resubmit your application for further review.', 'woocommerce' ); ?></p> 
<p><?php _e( 'We are committed to ensuring our platform remains exclusive to qualified retailers and appreciate your understanding in this matter.', 'woocommerce' ); ?></p>

<p><?php _e( 'Should you have any questions or need assistance with the resubmission process, please do not hesitate to contact our support team.', 'woocommerce' ); ?></p>

<p><?php _e( 'Sincerely,', 'woocommerce' ); ?></p>
<p><?php _e( 'Client Support Team', 'woocommerce' ); ?></p>
<p><?php _e( 'apM MUST', 'woocommerce' ); ?></p>
<p><?php _e( 'hello@apmmust.com', 'woocommerce' ); ?></p>

<?php
do_action( 'woocommerce_email_footer', $email );