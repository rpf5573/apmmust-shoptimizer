<?php
/**
 * User Group Temporary Approved Email Template.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/user-group-temporary.php.
 */

defined('ABSPATH') || exit;

do_action('woocommerce_email_header', $email_heading, $email);

// $user_group 변수 값에 따라 다른 그룹 이름을 할당합니다.
switch ($user_group) {
    case 'temporary_asia':
        $user_group_name = 'Temporary Asia';
        break;
    case 'temporary_global':
        $user_group_name = 'Temporary Global';
        break;
    default:
        $user_group_name = $user_group; // 기본적으로 변수의 원래 값을 사용합니다.
}

?>
<p>
    <?php echo sprintf(__("We are delighted to inform you that your account has been successfully re-activated within the %s.", 'woocommerce'), '<strong>' . esc_html( $user_group_name) . '</strong>' ); ?>
</p>

<p>
    <?php _e('We eagerly anticipate your participation and are here to support your needs on our platform.', 'woocommerce'); ?>
</p>
<p>
    <?php _e('Please note that in order to maintain an active status within our community, we require that at least one order be placed within one month from the date of re-activation. Accounts not meeting this criterion will be subject to deactivation.', 'woocommerce'); ?>
</p>
<p>
    <?php _e('Best regards,', 'woocommerce'); ?>
</p>
<p>
    <?php _e('Client Support Team', 'woocommerce'); ?>
</p>
<p>
    <?php _e('apM MUST', 'woocommerce'); ?>
</p>
<p>
    <?php _e('hello@apmmust.com', 'woocommerce'); ?>
</p>

<?php
do_action('woocommerce_email_footer', $email);
