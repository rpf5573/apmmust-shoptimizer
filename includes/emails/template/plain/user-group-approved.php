<?php
/**
 * User Group Approved Email Template - Plain Text.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/plain/user-group-approved.php.
 */

defined( 'ABSPATH' ) || exit;

echo $email_heading . "\n\n";

_e( "Congratulations! Your user group has been approved. You are now part of the exclusive group: {$user_group}.", 'woocommerce' );

echo "\n\n";

_e( 'You can now enjoy special benefits and privileges exclusive to your group. Thank you for being with us.', 'woocommerce' );

echo "\n\n";

