<?php
/**
 * User Group Disapproved Plain Email Template.
 */

defined( 'ABSPATH' ) || exit;

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
echo wp_strip_all_tags( $email_heading );
echo "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo sprintf( __("Dear %s,", 'woocommerce'), $user->display_name ) . "\n\n";

echo __( ' We regret to inform you that your account application has been declined following our review process. Based on the information provided, such as your business license and shop URL, we were unable to confirm your status as a retailer. This decision was made with careful consideration, and we understand the inconvenience this may cause.', 'woocommerce' ) . "\n\n";

echo __( 'However, should you be able to provide documentation that clearly verifies your status as a retailer, we welcome you to resubmit your application for further review. We are committed to ensuring our platform remains exclusive to qualified retailers and appreciate your understanding in this matter.', 'woocommerce' ) . "\n\n";

echo __( 'Should you have any questions or need assistance with the resubmission process, please do not hesitate to contact our support team.', 'woocommerce' ) . "\n";

echo "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n";
