<?php
/**
 * User Group Deactivated Email Template.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/user-group-deactivated.php.
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( "We regret to inform you that your account has been deactivated due to a lack of orders over the past months.", 'woocommerce' ); ?></p>

<p><?php _e( 'This measure is part of our commitment to safeguarding the interests of the wholesale brands we represent.', 'woocommerce' ); ?></p>
<p><?php _e( 'We understand this may be inconvenient and are here to address any questions or concerns you may have regarding this decision or how you can reactivate your account in the future.', 'woocommerce' ); ?></p>
<p><?php _e( 'Please feel free to contact us should you need any further information or assistance.', 'woocommerce' ); ?></p>

<p><?php _e( 'Sincerely,', 'woocommerce' ); ?></p>
<p><?php _e( 'Client Support Team', 'woocommerce' ); ?></p>
<p><?php _e( 'apM MUST', 'woocommerce' ); ?></p>
<p><?php _e( 'hello@apmmust.com', 'woocommerce' ); ?></p>

<?php
do_action( 'woocommerce_email_footer', $email );
