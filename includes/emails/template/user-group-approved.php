<?php
/**
 * User Group Approved Email Template.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/user-group-approved.php.
 */

defined('ABSPATH') || exit;

do_action('woocommerce_email_header', $email_heading, $email);

// $user_group 변수 값에 따라 다른 그룹 이름을 할당합니다.
switch ($user_group) {
    case 'b2b_global':
        $user_group_name = 'B2B Global';
        break;
    case 'b2b_asia':
        $user_group_name = 'B2B Asia';
        break;
    case 'b2b_kz_kg':
        $user_group_name = 'B2B KZ KG';
        break;
    case 'b2b_ru':
        $user_group_name = 'B2B RU';
        break;
    default:
        $user_group_name = $user_group; // 기본적으로 변수의 원래 값을 사용합니다.
}

?>

<p>
    <?php echo sprintf(__("Thank you for your patience during our review process. We are pleased to inform you that your account has met our platform's eligibility criteria and has been successfully approved as part of %s.", 'woocommerce'), '<strong>' . esc_html( $user_group_name) . '</strong>' ); ?>
</p>

<p>
    <?php _e('Should you have any questions or require further assistance, please do not hesitate to reach out to us. We are here to support you and ensure your experience with our platform is as seamless and beneficial as possible.', 'woocommerce'); ?>
</p>

<p>
    <?php _e('Warm regards,', 'woocommerce'); ?>
</p>
<p>
    <?php _e('Client Support Team', 'woocommerce'); ?>
</p>
<p>
    <?php _e('apM MUST', 'woocommerce'); ?>
</p>
<p>
    <?php _e('hello@apmmust.com', 'woocommerce'); ?>
</p>

<?php
do_action('woocommerce_email_footer', $email);
