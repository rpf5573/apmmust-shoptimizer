<?php
// apM MUST 이메일 클래스 2024.02.11
// 이 코드는 must_email.php에 위치한다.
// 모든 이메일 관련 파일들은 /var/www/html/wp-content/themes/shoptimizer-child-theme/includes/emails안에 위치
// 각 이메일 템플레이트도 /var/www/html/wp-content/themes/shoptimizer-child-theme/includes/emails/template에 위치하고
// Plain은 /var/www/html/wp-content/themes/shoptimizer-child-theme/includes/emails/template/plain에 위치

function add_user_group_updated_email($email_classes)
{
  include_once 'class-wc-user-group-approved-email.php'; // 승인 이메일 클래스
  include_once 'class-wc-user-group-disapproved-email.php'; // 거부 이메일 클래스
  include_once 'class-wc-user-group-deactivated-email.php'; // 비활성화 이메일 클래스
  include_once 'class-wc-user-group-temporary-email.php'; // 임시승인 이메일 클래스

  $email_classes['WC_User_Group_Approved_Email'] = new WC_User_Group_Approved_Email();
  $email_classes['WC_User_Group_Disapproved_Email'] = new WC_User_Group_Disapproved_Email();
  $email_classes['WC_User_Group_Temporary_Email'] = new WC_User_Group_Temporary_Email();
  $email_classes['WC_User_Group_Deactivated_Email'] = new WC_User_Group_Deactivated_Email();

  return $email_classes;
}
add_filter('woocommerce_email_classes', 'add_user_group_updated_email');

// 사용자의 그룹 체크 및 이메일 발송 작업 예약
add_action('updated_user_meta', 'check_user_group_update_and_schedule_email', 10, 4);
function check_user_group_update_and_schedule_email($meta_id, $user_id, $meta_key, $meta_value)
{
  if ('user_group' === $meta_key) {
    // 이전 그룹 값을 메타 데이터에서 가져옵니다.
    $old_group = get_user_meta($user_id, 'user_group_old_value', true);

    // 현재 그룹과 이전 그룹이 동일하다면, 변경이 없으므로 함수를 종료합니다.
    if ($old_group === $meta_value) {
      error_log('No change in user group for User ID=' . $user_id . '. No email will be sent.');
      return;
    }

    // 변경이 감지된 경우, 로그를 남기고 이메일 발송 작업을 예약합니다.
    error_log('User ID=' . $user_id . ' group change detected: From ' . $old_group . ' to ' . $meta_value);


    $b2b_groups = array('b2b_global', 'b2b_asia', 'b2b_kz_kg', 'b2b_ru');
    $temporary_groups = array('temporary_asia', 'temporary_global');

    // 'pending'에서 특정 'b2b_' 그룹으로 변경
    if (in_array($meta_value, $b2b_groups) && $old_group === 'pending') {
      // 여기에 'group' 정보를 추가하여 전달합니다.
      as_schedule_single_action(time(), 'user_group_approved_notification', ['user_id' => $user_id, 'group' => $meta_value]);
      error_log('Scheduled action: user_group_approved_notification for User ID=' . $user_id . ', group=' . $meta_value);
    }

    // 'pending'에서 'disapproved'로 변경
    elseif ($meta_value === 'disapproval' && $old_group === 'pending') {
      as_schedule_single_action(time(), 'user_group_disapproved_notification', ['user_id' => $user_id]);
      error_log('Scheduled action: user_group_disapproved_notification for User ID=' . $user_id);
    }
    // 모든 그룹에서 'temporary_' 그룹으로 변경
    elseif (in_array($meta_value, $temporary_groups)) {
      as_schedule_single_action(time(), 'user_group_temporary_notification', ['user_id' => $user_id, 'group' => $meta_value]);
      error_log('Scheduled action: user_group_temporary_notification for User ID=' . $user_id);
    }
    // 'deactivated'로 변경
    elseif ($meta_value === 'deactivated') {
      as_schedule_single_action(time(), 'user_group_deactivated_notification', ['user_id' => $user_id]);
      error_log('Scheduled action: user_group_deactivated_notification for User ID=' . $user_id);
    } else {
      // 이전 그룹과 새 그룹이 같거나, 다른 조건에 해당하지 않는 경우
      error_log('No specific action scheduled for user group change for User ID=' . $user_id);
    }
  }
}

// 이메일 발송 콜백 함수 - 승인
function handle_user_group_approved_notification($args)
{
  // 'group' 키가 $args 배열에 존재하는지 확인
  if (!is_array($args) || !isset($args['group'])) {
    error_log('Error: The required "group" key is missing in the args array.');
    return; // 'group' 정보가 없으므로 함수를 여기서 종료
  }

  $user_id = $args['user_id'];
  $group = $args['group']; // 'group' 키가 존재한다고 가정하고 접근
  $user = new WP_User($user_id);

  if (isset(WC()->mailer()->emails['WC_User_Group_Approved_Email'])) {
    $email = WC()->mailer()->emails['WC_User_Group_Approved_Email'];
    $email->recipient = $user->user_email;
    $email->user_group = $group;
    if ($email->trigger($user_id, $group)) {
      error_log('User group approved email sent successfully to User ID=' . $user_id);
    } else {
      error_log('Failed to send user group approved email to User ID=' . $user_id);
    }
  } else {
    error_log('WC_User_Group_Approved_Email class is not registered in WooCommerce emails.');
  }
}


// 이메일 발송 콜백 함수 - 거부
function handle_user_group_disapproved_notification($args)
{
  if (!is_array($args)) {
    $args = ['user_id' => $args]; // 배열로 변환
  }
  $user_id = $args['user_id'];
  $user = new WP_User($user_id);
  if (isset(WC()->mailer()->emails['WC_User_Group_Disapproved_Email'])) {
    $email = WC()->mailer()->emails['WC_User_Group_Disapproved_Email'];
    $email->recipient = $user->user_email;
    $email->trigger($user_id);
  } else {
    error_log('WC_User_Group_Disapproved_Email class is not registered in WooCommerce emails.');
  }
}
add_action('user_group_disapproved_notification', 'handle_user_group_disapproved_notification', 10, 1);

// 이메일 발송 콜백 함수 - 임시 재승인
function handle_user_group_temporary_notification($args)
{
  if (!is_array($args)) {
    $args = ['user_id' => $args, 'group' => '']; // 'group' 인자를 기본값으로 추가
  }
  $user_id = $args['user_id'];
  $group = $args['group']; // 이 부분이 추가되어야 합니다.
  $user = new WP_User($user_id);
  if (isset(WC()->mailer()->emails['WC_User_Group_Temporary_Email'])) {
    $email = WC()->mailer()->emails['WC_User_Group_Temporary_Email'];
    $email->recipient = $user->user_email;
    // 아래 줄에서 $group 인자를 추가하여 함수 호출
    if ($email->trigger($user_id, $group)) {
      error_log('Temporary user group email sent successfully to User ID=' . $user_id);
    } else {
      error_log('Failed to send temporary user group email to User ID=' . $user_id);
    }
  } else {
    error_log('WC_User_Group_Temporary_Email class is not registered in WooCommerce emails.');
  }
}


// 이메일 발송 콜백 함수 - 비활성화
function handle_user_group_deactivated_notification($args)
{
  if (!is_array($args)) {
    $args = ['user_id' => $args]; // 배열로 변환
  }
  $user_id = $args['user_id'];
  $user = new WP_User($user_id);
  if (isset(WC()->mailer()->emails['WC_User_Group_Deactivated_Email'])) {
    $email = WC()->mailer()->emails['WC_User_Group_Deactivated_Email'];
    $email->recipient = $user->user_email;
    $email->trigger($user_id);
  } else {
    error_log('WC_User_Group_Deactivated_Email class is not registered in WooCommerce emails.');
  }
}
add_action('user_group_deactivated_notification', 'handle_user_group_deactivated_notification', 10, 1);
