<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class WC_User_Group_Approved_Email extends WC_Email {

    public function __construct() {
        // 이메일 아이디, 타이틀 설정
        $this->id    = 'wc_user_group_approved';
        $this->title = 'User Group Approved';

        // 이메일 설명
        $this->description = 'This email is sent to a user when their user group is approved.';

        // 이메일 템플릿 경로 설정 (HTML & Plain)
        $this->template_html  = '/user-group-approved.php';
        $this->template_plain = '/plain/user-group-approved.php';

        // 이메일 타입 설정
        $this->customer_email = true; // Send to customer

        // 이메일 템플릿 기본 경로 지정
        $this->template_base = '/var/www/html/wp-content/themes/shoptimizer-child-theme/includes/emails/template/';

        // Trigger on user group approved action
        add_action( 'user_group_approved_notification', array( $this, 'trigger' ), 10, 2 );

        // Call parent constructor to load any other defaults not specifically defined here
        parent::__construct();
    }

    public function trigger( $user_id, $group ) {
        $this->object = new WP_User( $user_id );
        $this->user_group = $group;

        if ( $this->object->user_email ) {
            $this->recipient = $this->object->user_email;
            $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
        }
    }

    public function get_content_html() {
        ob_start();
        wc_get_template( $this->template_html, array(
            'email_heading' => $this->get_heading(),
            'user_group'    => $this->user_group,
            'email'         => $this,
        ), '', $this->template_base );
        return ob_get_clean();
    }

    public function get_content_plain() {
        ob_start();
        wc_get_template( $this->template_plain, array(
            'email_heading' => $this->get_heading(),
            'user_group'    => $this->user_group,
            'email'         => $this,
        ), '', $this->template_base );
        return ob_get_clean();
    }

    public function get_subject() {
        return $this->format_string( $this->get_option( 'subject', 'Your account has been approved!' ) );
    }

    public function get_heading() {
        return $this->format_string( $this->get_option( 'heading', 'Approved' ) );
    }
}
