<?php
// apM MUST GA4 이벤트 전환 관리 2024.02.15

// 제품 조회 추적
function track_product_view()
{
  global $product;
  ?>
  <script>
    gtag('event', 'view_item', {
      "items": [{
        "id": "<?php echo $product->get_id(); ?>",
        "name": "<?php echo $product->get_name(); ?>",
        "category": "<?php echo strip_tags(wc_get_product_category_list($product->get_id())); ?>",
        "price": "<?php echo $product->get_price(); ?>"
      }]
    });
  </script>
  <?php
}
add_action('woocommerce_after_single_product', 'track_product_view');

// 결제 진행 상황 추적
function track_checkout_progress()
{
  if (is_checkout() && !is_order_received_page()) {
    ?>
    <script>
      gtag('event', 'begin_checkout', {
        // 필요하다면 추가 이벤트 파라미터 포함
      });
    </script>
    <?php
  }
}
add_action('wp_footer', 'track_checkout_progress');


// 검색 추적
function track_site_search()
{
  if (is_search() && get_search_query()) {
    ?>
    <script>
      gtag('event', 'search', {
        'search_term': '<?php echo get_search_query(); ?>'
      });
    </script>
    <?php
  }
}
add_action('wp_footer', 'track_site_search');

// 환불 추적
function track_refund($order_id)
{
  ?>
  <script>
    gtag('event', 'refund', {
      "transaction_id": "<?php echo $order_id; ?>",
      // 필요하다면 추가 환불 데이터 포함
    });
  </script>
  <?php
}
add_action('woocommerce_order_status_refunded', 'track_refund');


// 장바구니에 추가 액션 추적
function track_add_to_cart()
{
  global $woocommerce;
  ?>
  <script>
    jQuery(function ($) {
      $(document).on('added_to_cart', function (event, fragments, cart_hash, $button) {
        var id = $button.data('product_id');
        var name = $button.data('product_name');
        var price = $button.data('product_price');
        var quantity = $button.data('quantity');

        gtag('event', 'add_to_cart', {
          "items": [{
            "id": id.toString(),
            "name": name,
            "price": price.toString(),
            "quantity": quantity
          }]
        });
      });
    });
  </script>
  <?php
}
add_action('wp_footer', 'track_add_to_cart');

// 구매 추적 및 항목 상세 정보 추가
function track_purchase($order_id)
{
  $order = wc_get_order($order_id);
  $items = $order->get_items();
  ?>
  <script>
    gtag('event', 'purchase', {
      "transaction_id": "<?php echo esc_js($order_id); ?>",
      "value": <?php echo esc_js($order->get_total()); ?>,
      "currency": "<?php echo esc_js($order->get_currency()); ?>",
      "items": [
        <?php foreach ($items as $item): ?>
                {
            "id": "<?php echo esc_js($item->get_product_id()); ?>",
            "name": "<?php echo esc_js($item->get_name()); ?>",
            "quantity": "<?php echo esc_js($item->get_quantity()); ?>",
            "price": "<?php echo esc_js($item->get_subtotal()); ?>"
          },
        <?php endforeach; ?>
      ]
    });
  </script>
  <?php
}
add_action('woocommerce_thankyou', 'track_purchase');

// 브랜드 및 상품 조회 추적
function track_product_and_brand_view()
{
  global $product;

  // 상품 상세 페이지에서 브랜드 조회 추적
  if (is_product()) {
    $product_id = $product->get_id();
    $brand_terms = wp_get_post_terms($product_id, 'product_brand');

    if (!empty($brand_terms) && !is_wp_error($brand_terms)) {
      // 첫 번째 브랜드를 사용. 필요에 따라 조정 가능
      $brand_name = $brand_terms[0]->name;
      ?>
      <script>
        gtag('event', 'view_item', {
          "items": [{
            "id": "<?php echo esc_js($product_id); ?>",
            "brand": "<?php echo esc_js($brand_name); ?>"
          }]
        });
      </script>
      <?php
    }
  }

  // 브랜드 페이지 조회 추적
  if (is_tax('product_brand')) {
    $brand = single_term_title('', false);
    ?>
    <script>
      gtag('event', 'brand_view', {
        'brand_name': '<?php echo esc_js($brand); ?>'
      });
    </script>
    <?php
  }
}
add_action('wp_footer', 'track_product_and_brand_view');
