<?php

// Insert Base GA4 Tracking Code
function insert_ga4_base_code() {
    ?>
    <!-- First GA4 Property -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-1BL627E6WB"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-1BL627E6WB');
    </script>

    <!-- Second GA4 Property -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-YCT4YWJGQZ"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-YCT4YWJGQZ');
    </script>
    <?php
}
add_action('wp_head', 'insert_ga4_base_code');

