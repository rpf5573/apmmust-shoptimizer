<?php

function insert_ga4_base_code() {
    // 클라우드플레어에서 방문자의 국가 코드를 가져옵니다.
    $user_country = $_SERVER["HTTP_CF_IPCOUNTRY"] ?? '';

    // South Korea(KR) 방문자가 아닐 경우에만 GA4 추적 코드를 삽입합니다.
    if ($user_country !== "KR") {
        ?>
        <!-- First GA4 Property -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-1BL627E6WB"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-1BL627E6WB');
        </script>

        <!-- Second GA4 Property -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-YCT4YWJGQZ"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-YCT4YWJGQZ');
        </script>
        <?php
    }
}
add_action('wp_head', 'insert_ga4_base_code');
