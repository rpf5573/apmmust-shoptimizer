<?php
// apM MUST Vendor 브랜드 관련 필수 코드
// 새로운 컬럼 추가
function add_brand_columns($columns) {
  $columns['room_number'] = '방 번호';
  $columns['floor'] = '층';
  $columns['building'] = '건물';
  return $columns;
}
add_filter('manage_edit-product_brand_columns', 'add_brand_columns');


// 각 컬럼에 데이터 출력
function display_brand_columns_data($deprecated, $column_name, $term_id) {
  if ($column_name === 'room_number' || $column_name === 'floor' || $column_name === 'building') {
      echo get_brand_linked_meta_data($term_id)[array_search($column_name, ['room_number', 'floor', 'building'])];
  }
}
add_action('manage_product_brand_custom_column', 'display_brand_columns_data', 10, 3);


// 메타데이터 가져오기
function get_brand_linked_meta_data($object_id) {
  $room_number_field_id = 'brand_linked_room_number';
  $floor_field_id = 'brand_linked_floor';
  $building_field_id = 'brand_linked_building';
  
  $room_number = rwmb_meta($room_number_field_id, array('object_type' => 'term'), $object_id);
  $floor = rwmb_meta($floor_field_id, array('object_type' => 'term'), $object_id);
  $building = rwmb_meta($building_field_id, array('object_type' => 'term'), $object_id);
  
  return array($room_number, $floor, $building);
}

function show_desktop_nav_menu() {
  echo do_shortcode('[desktop_nav_menu]');
}

function apmmust_get_menu_item_children($menu_items, $parent_id = 0) {
  $children = array();

  foreach ($menu_items as $item) {
    if ($item->menu_item_parent == $parent_id) {
      $child = array(
        'name' => $item->title,
        'slug' => $item->post_name,
        'id'   => $item->ID,
        'href' => $item->url,
        'description' => trim($item->description),
        'children' => apmmust_get_menu_item_children($menu_items, $item->ID) // 재귀 호출
      );
      $children[] = $child;
    }
  }

  return $children;
}

function apmmust_get_menu_item_children_filtered($children, $filter_names) {
  return array_filter($children, function ($child) use ($filter_names) {
    return in_array(strtolower($child['name']), $filter_names);
  });
}

function apmmust_get_primary_menu_item_list_with_children() {
  $locations = get_nav_menu_locations();
  $menu_id = isset($locations['primary']) ? $locations['primary'] : '';

  if (!empty($menu_id)) {
    $menu_items = wp_get_nav_menu_items($menu_id);
    $menu_items_with_children = apmmust_get_menu_item_children($menu_items);
  }

  return $menu_items_with_children;
}

function apmmust_get_product_count_by_slug($category_slug, $days) {
  // Check for a cached version of the count
  $cache_key = 'product_count_' . $category_slug . '_' . $days;
  $cached_count = get_transient($cache_key);
  
  if ($cached_count !== false) {
    return $cached_count; // Return the cached count if it exists
  }

  global $wpdb;

  // Get the top-level category by slug
  $top_level_category = get_term_by('slug', $category_slug, 'product_cat');
  if (!$top_level_category) {
    return 0; // Return zero if category not found
  }

  // Get IDs of all descendant categories
  $category_ids = get_term_children($top_level_category->term_id, 'product_cat');
  // Include the top-level category itself
  $category_ids[] = $top_level_category->term_id;

  // Prepare SQL query to count products
  $query = "
    SELECT COUNT(DISTINCT posts.ID)
    FROM {$wpdb->posts} AS posts
    INNER JOIN {$wpdb->term_relationships} AS tr ON posts.ID = tr.object_id
    INNER JOIN {$wpdb->term_taxonomy} AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
    WHERE posts.post_status = 'publish'
    AND posts.post_type IN ('product', 'product_variation')
    AND tt.term_id IN (" . implode(',', array_map('intval', $category_ids)) . ")
    AND posts.post_date >= %s
    AND posts.post_date <= %s
  ";

  // Format the dates for the query
  $date_after = date('Y-m-d 00:00:00', strtotime("-{$days} days"));
  $date_before = date('Y-m-d 23:59:59', strtotime('today'));

  // Prepare the query with the date parameters
  $prepared_query = $wpdb->prepare($query, $date_after, $date_before);

  // Execute the query and get the product count
  $product_count = $wpdb->get_var($prepared_query);

  // Cache the count with a transient, set to expire in 1 hours
  set_transient($cache_key, $product_count, 1 * HOUR_IN_SECONDS);

  return $product_count;
}

function apmmust_get_new_vendors($limit = 13) {
  $field_id = 'vendor_store_brand_term'; 

  // Get users with 'apmmust_vendor' role
  $args = array(
    'role'    => 'apmmust_vendor',
    'orderby' => 'registered',
    'order'   => 'DESC',
    'number'  => $limit,
    'fields'  => 'all_with_meta' // This will include user meta in the results
  );

  $vendors = get_users($args);  

  // Create an array to hold vendor data including the brand from user meta
  $vendor_list = array();
  
  foreach ($vendors as $vendor) {
    $brand = rwmb_meta($field_id, array('object_type' => 'user'), $vendor->ID);
    $created_at = $vendor->user_registered;
    $vendor_list[] = array(
      'ID' => $vendor->ID,
      'brand_term' => $brand,
      'created_at' => $created_at
    );
  }

  return $vendor_list;
}

function apmmust_get_new_brand_term_list($limit = 15) {
  $vendors = apmmust_get_new_vendors($limit);
  $brand_term_list = array_filter(array_map(function ($vendor) {
    return $vendor['brand_term'];
  }, $vendors));
  
  $brand_term_list = array_map(function ($term) {
    $term_link = get_term_link($term->term_id, 'product_brand');
    return array(
      'id' => $term->term_id,
      'name' => $term->name,
      'slug' => $term->slug,
      'term_link' => $term_link,
    );
  }, $brand_term_list);

  return $brand_term_list;
}

function apmmust_get_brand_image_url_by_term($term) {
  $id = $term->term_id;
  $image = rwmb_meta( 'featured_brand_image', [ 'object_type' => 'term', 'size' => 'thumbnail' ], $id );
  return $image['url'];
}

function apmmust_register_vendor_list_menu() {
  register_nav_menu('vendor_list', __( 'Vendor List' ));
}
add_action('init', 'apmmust_register_vendor_list_menu');

function apmmust_get_featured_brand_list() {
  $featured_brand_term_list = rwmb_meta(
    "featured_brands",
    ["object_type" => "setting"],
    "apmmust-custom-settings"
  );
  
  if (empty($featured_brand_term_list)) {
    return array();
  }

  $featured_brand_list = array_map(function ($term) {
    $term_id = $term->term_id;
    $term_name = $term->name;
    $term_image = rwmb_meta('featured_brand_image', ['object_type' => 'term'], $term_id);
    $term_image_url = isset($term_image['full_url']) ? $term_image['full_url'] : '';
    $term_link = get_term_link($term_id, 'product_brand');

    return array(
      'id' => $term_id,
      'name' => $term_name,
      'img_url' => $term_image_url,
      'term_link' => $term_link,
    );
  }, $featured_brand_term_list);

  return $featured_brand_list;
}

function apmmust_get_brand_hierarchy() {
  $cache_key = 'apmmust_brand_hierarchy';
  $cached_hierarchy = get_transient($cache_key);

  if (!APMMUST_BYPASS_CACHE) {
    // 캐시된 데이터가 있으면 바로 반환
    if ($cached_hierarchy !== false) {
      return $cached_hierarchy;
    }
  }

  $terms = get_terms(array(
      'taxonomy'   => 'product_brand',
      'post_type'  => 'product',
      'hide_empty' => false,
      'parent'     => 0
  ));

  $brand_hierarchy = array();

  foreach ($terms as $term) {
      $brand_hierarchy[] = apmmust_build_hierarchy($term);
  }

  // 계산된 브랜드 계층 구조를 캐시에 저장
  set_transient($cache_key, $brand_hierarchy, DAY_IN_SECONDS);

  return $brand_hierarchy;
}

function apmmust_build_hierarchy($term, $hierarchy = array()) {
  $term_link = get_term_link($term);
  if (is_wp_error($term_link)) {
      return $hierarchy;
  }

  $children = get_terms(array(
      'taxonomy'   => $term->taxonomy,
      'parent'     => $term->term_id,
      'hide_empty' => false
  ));

  $child_hierarchy = array();

  foreach ($children as $child) {
      $child_hierarchy[] = apmmust_build_hierarchy($child);
  }

  $hierarchy = array(
      'term_id'   => $term->term_id,
      'name'      => $term->name,
      'href' => $term_link,
      'children'  => $child_hierarchy
  );

  return $hierarchy;
}

function apmmust_get_all_brands() {
  $cache_key = 'apmmust_all_brands';
  $cached_brands = get_transient($cache_key);

  if (!APMMUST_BYPASS_CACHE) {
    // 캐시된 데이터가 있으면 바로 반환
    if ($cached_brands !== false) {
        return $cached_brands;
    }
  }

  $terms = get_terms(array(
      'taxonomy'   => 'product_brand',
      'post_type'  => 'product',
      'hide_empty' => false
  ));

  $brands = array();
  foreach ($terms as $term) {
      $brands[] = array(
          'term_id' => $term->term_id,
          'name'    => $term->name,
          'slug'    => $term->slug,
          'href'    => get_term_link($term)
      );
  }

  // 결과를 캐시에 저장
  set_transient($cache_key, $brands, DAY_IN_SECONDS);

  return $brands;
}

function apmmust_get_buildings() {
  $cache_key = 'apmmust_buildings';
  $cached_buildings = get_transient($cache_key);

  if (!APMMUST_BYPASS_CACHE) {
    // 캐시된 데이터가 있으면 바로 반환
    if ($cached_buildings !== false) {
      return $cached_buildings;
    }
  }
  
  $brands = apmmust_get_all_brands();
  $buildings = array_reduce($brands, function ($acc, $cur) {
    $term_id = $cur['term_id'];
    $is_building = rwmb_meta('is_this_building', ['object_type' => 'term'], $term_id);
    if ($is_building) {
      $acc[] = $cur;
    }
    return $acc;
  }, []);

  // 결과를 캐시에 저장
  set_transient($cache_key, $buildings, DAY_IN_SECONDS);

  return $buildings;
  }