<?php
// brand taxonomy에 hierarchical 옵션을 설정하는 코드
// 상위 브랜드를 설정할 수 있게됩니다
// 상위 브랜드 설정 코드 - 시작
add_filter('register_product_brand_taxonomy_args', 'apmmust_change_product_brand_taxonomy_args');
function apmmust_change_product_brand_taxonomy_args($args)
{
  $args['hierarchical'] = true;
  return $args;
}
// 상위 브랜드 설정 코드 - 끝