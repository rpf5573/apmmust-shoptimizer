<?php

add_shortcode("brand_list", function () {
  ob_start();
  $brand_hierarchy = apmmust_get_brand_hierarchy();
  $building_hierarchy = array_filter($brand_hierarchy, function ($brand) {
	$term_id = $brand["term_id"];
	$is_building = rwmb_meta('is_this_building', ['object_type' => 'term'], $term_id);
	if ($is_building) {
		return true;
	}
	return false;
  });


  $all_brand_list = array_reduce($building_hierarchy, function($acc, $cur) {
	$children = $cur['children'];
	if (!empty($children)) {
		foreach($children as $child) {
			$child_children = $child['children'];
			if (!empty($child_children)) {
				foreach($child_children as $child_child) {
					if (empty($child_child['children'])) {
						$acc[] = $child_child;
					} else {
						foreach($child_child['children'] as $child_child_child) {
							$acc[] = $child_child_child;
						}
					}
				}
			}
		}
	}
	return $acc;
  }, []);
  ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.5.0/semantic.min.css" integrity="sha512-KXol4x3sVoO+8ZsWPFI/r5KBVB/ssCGB5tsv2nVOKwLg33wTFP3fmnXa47FdSVIshVTgsYk/1734xSk9aFIa4A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.5.0/components/accordion.min.css" integrity="sha512-EW5NoIdxRt4Kx9yB4sh9TKVYOveAOFf8WwjRwQs4ylh1hDueujFGLJtPNjm4zQKwlPk8Q2mYDLte7aK6NS+uoA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<style>
	#primary {
		float: none;
	}
	.brand-list {
		display: grid; /* 그리드 레이아웃 사용 */
		grid-template-columns: repeat(6, 1fr); /* 6개의 동일한 너비의 열 정의 */
		gap: 10px; /* 그리드 항목 간의 공간 설정 */
	}

	@media (max-width: 1200px) {
		#primary .brand-list {
			grid-template-columns: repeat(4, 1fr); /* 화면 너비가 1200px 이하일 때 4열로 표시 */
		}
	}

	@media (max-width: 980px) {
		#primary .brand-list {
			grid-template-columns: repeat(3, 1fr); /* 화면 너비가 980px 이하일 때 3열로 표시 */
		}
	}

	@media (max-width: 768px) {
		#primary .brand-list {
			grid-template-columns: repeat(2, 1fr); /* 화면 너비가 768px 이하일 때 2열로 표시 */
		}
	}

	#primary .ui.styled.accordion {
		width: 1400px;
	}
	#primary .ui.accordion > .title {
		font-size: 20px;
	}
	#primary label[for="id_label_single"] {
		display: block;
		width: 35%;
		margin-bottom: 20px;
	}

	.brand-list-item a {
		color: black;
	}

	.brand-list-item a:hover {
		color: gray;
	}

	@media (max-width: 768px) {
		#primary label[for="id_label_single"] {
			width: 60%;
		}
	}
</style>

<div class="cont_wrap">
	<div class="account_wrap">
			<div class="vendor_wrap">
				<label for="id_label_single">
					Click and search brand
					<select class="js-example-basic-single js-states form-control" id="id_label_single">
						<?php
						foreach($all_brand_list as $brand) { ?>
							<option value="<?php echo $brand['name']; ?>" data-href="<?php echo $brand['href']; ?>"><?php echo $brand['name']; ?></option> <?php
						} ?>
					</select>
				</label>
		</div>
	</div>
	<div class="building-list-container">
		<div class="ui styled accordion">
			<?php
			foreach($building_hierarchy as $building) { ?>
				<div class="active title">
					<i class="dropdown icon"></i>
					<?php echo $building['name']; ?>
				</div>
				<div class="active content">
					<?php
					$children = $building['children'];
					if (!empty($children)) { ?>
						<div class="accordion"> <?php
							foreach($children as $child) {
								$child_children = $child['children'];
								if (!empty($child_children)) { ?>
									<div class="title"><i class="dropdown icon"></i><?php echo $child['name']; ?></div>
									<div class="content">
										<ul class="brand-list"><?php
											foreach($child_children as $child_child) { ?>
												<li class="brand-list-item">
													<a href="<?php echo $child_child['href']; ?>"><?php echo $child_child['name']; ?></a>
												</li>
												<?php
											} ?>
										</ul>
									</div> <?php
								} else {

								}
							} ?>
						</div> <?php
					} else {}?>
				</div> <?php
			}
			?>
		</div>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.5.0/semantic.min.js" integrity="sha512-Xo0Jh8MsOn72LGV8kU5LsclG7SUzJsWGhXbWcYs2MAmChkQzwiW/yTQwdJ8w6UA9C6EVG18GHb/TrYpYCjyAQw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.5.0/components/accordion.min.js" integrity="sha512-VZ9OKywfKY7qvZnTAsFqNHS6jZ79QmSdfXbzoS3aMy3FpNkDFrR2NJfrHEE4nPQROp4A9u/hB9rTwL0UP5tzHg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
	document.addEventListener('DOMContentLoaded', function() {
		jQuery('.ui.accordion').accordion({
			'exclusive': false,
		});

		jQuery('.js-example-basic-single').select2({
			placeholder: 'Select an option'
		});

		jQuery('.js-example-basic-single').on('select2:select', function (e) {
			// get option's data-href attribute
			const dataHref = jQuery(e.params.data.element).data('href');
			dataHref && (window.location = dataHref);
		});
	});
</script>

<?php return ob_get_clean();
});