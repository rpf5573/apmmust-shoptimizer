<?php
//** Product Custom Fields for Korean name and Wholesale Price **//
// Display Fields
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields');
// Save Fields
add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');
function woocommerce_product_custom_fields()
{
  global $woocommerce, $post;
  echo '<div class="product_custom_field">';
  // Custom Product Text Field
  woocommerce_wp_text_input(
    array(
      'id' => '_custom_product_korean_name',
      'placeholder' => '',
      'label' => __('Korean Name', 'woocommerce'),
      'desc_tip' => 'true'
    )
  );
  //Custom Product Number Field
  woocommerce_wp_text_input(
    array(
      'id' => '_custom_product_wholesale_price',
      'placeholder' => '',
      'label' => __('Wholesale Price', 'woocommerce'),
      'type' => 'number',
      'custom_attributes' => array(
        'step' => 'any',
        'min' => '0'
      )
    )
  );
  echo '</div>';
}

function woocommerce_product_custom_fields_save($post_id)
{
  // Custom Product Text Field
  $woocommerce_custom_product_text_field = $_POST['_custom_product_korean_name'];
  if (!empty($woocommerce_custom_product_text_field))
    update_post_meta($post_id, '_custom_product_korean_name', esc_attr($woocommerce_custom_product_text_field));
  // Custom Product Number Field
  $woocommerce_custom_product_number_field = $_POST['_custom_product_wholesale_price'];
  if (!empty($woocommerce_custom_product_number_field))
    update_post_meta($post_id, '_custom_product_wholesale_price', esc_attr($woocommerce_custom_product_number_field));
}

//* Done

// 장바구니 페이지 접속 시 품절된 상품을 자동으로 제거하는 함수를 실행하기 위한 액션
add_action('woocommerce_before_cart', 'enhanced_remove_out_of_stock_products_from_cart');

function enhanced_remove_out_of_stock_products_from_cart() {
    global $wpdb; // WordPress 데이터베이스 클래스에 접근
    $user_id = get_current_user_id(); // 현재 로그인한 사용자의 ID를 가져옴

    if (WC()->cart->is_empty() || !$user_id) {
        return; // 장바구니가 비어있거나 사용자가 로그인하지 않은 경우 함수 종료
    }

    $removed_products = [];
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
      $product_obj = $cart_item['data'];
      if (!$product_obj->is_in_stock()) {
          WC()->cart->remove_cart_item($cart_item_key); // 품절된 상품을 장바구니에서 제거
          $removed_products[] = $product_obj->get_name(); // 제거된 상품 이름 수집
  
          // 부모 상품 ID를 확인하고 설정
          $product_id = $product_obj->is_type('variation') ? $product_obj->get_parent_id() : $product_obj->get_id();
  
          $current_time = current_time('timestamp'); // 현재 시간을 Unix 타임스탬프로 가져옴
  
          // 위시리스트에 이미 있는지 확인
          $exists = $wpdb->get_var($wpdb->prepare(
              "SELECT COUNT(*) FROM {$wpdb->prefix}commercekit_wishlist_items WHERE user_id = %d AND product_id = %d",
              $user_id, $product_id
          ));
  
          // 위시리스트에 없는 경우에만 추가
          if (!$exists) {
              $wpdb->insert(
                  "{$wpdb->prefix}commercekit_wishlist_items",
                  [
                      'user_id' => $user_id,
                      'list_id' => 0, // 로그인 사용자는 list_id를 0으로 설정
                      'product_id' => $product_id,
                      'created' => $current_time, // 추가된 시간
                  ],
                  ['%d', '%d', '%d', '%d']
              );
          }
      }
  }
}

add_filter( 'woocommerce_add_error', 'customize_woocommerce_wishlist_error', 10, 1 );
function customize_woocommerce_wishlist_error( $error ) {
    // 원하는 조건을 검사하여 특정 에러 메시지가 맞는지 확인
    if ( strpos( $error, 'is not in stock. Please edit your cart and try again. We apologize for any inconvenience caused.' ) !== false ) {
        // 원하는 메시지로 수정
        $error = "The out-of-stock product has been removed from the cart and added to the wishlist.";
    }
    return $error;
}
