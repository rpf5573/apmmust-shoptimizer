<?php
// apM MUST 상품 그룹화 및 최소수량 2개 (양말 카테고리 10개) 2024.02.09
// 코드 연산 최적화 적용 2024.02.09 (윤개발자님 개발 코드를 최적화함.)

// 상품 데이터 캐싱을 위한 글로벌 변수
$GLOBALS['apm_product_cache'] = [];

// 상품 variation 관계없이 같은 상품이면 그룹핑하고, 갯수도 알려준다.
function apmmust_group_products_that_are_the_same_regardless_of_product_variation_and_count($cart_items) {
  // array_reduce를 사용하여 카트 아이템 배열을 상품 ID 별로 그룹화하고 수량을 합산
  return array_reduce($cart_items, function($carry, $cart_item) {
      $product_id = $cart_item['product_id'];
      
      // 상품 데이터 캐싱 로직 적용
      if (!isset($GLOBALS['apm_product_cache'][$product_id])) {
          $GLOBALS['apm_product_cache'][$product_id] = $cart_item['data']->get_name();
      }
      $product_name = $GLOBALS['apm_product_cache'][$product_id];

      // 상품 ID가 이미 배열에 존재하면 수량을 합산, 그렇지 않으면 새로운 항목 추가
      if (!isset($carry[$product_id])) {
          $carry[$product_id] = ['count' => 0, 'name' => $product_name];
      }
      $carry[$product_id]['count'] += intval($cart_item['quantity']);

      return $carry;
  }, []);
}


// 에러 메시지 관리를 위한 함수 (세션 데이터 관리 최적화)
function apmmust_manage_notices_optimized($message = null, $reset = false) {
    static $notices = null; // 로컬 캐싱을 위한 정적 변수

    if ($reset) {
        $notices = array('error' => array()); // 에러 메시지 초기화
    } elseif ($message !== null) {
        if ($notices === null) {
            $notices = WC()->session->get('wc_notices', array('error' => array()));
        }
        $notices['error'][] = array('notice' => $message, 'data' => 'apmmust_error');
    }

    if ($notices !== null) {
        WC()->session->set('wc_notices', $notices);
    }
}

// 상품 수량 검증 로직을 공통 함수로 추출 (주요 액션 훅 내 로직 최적화)
function apmmust_validate_product_quantities($cart_items) {
    $products = apmmust_group_products_that_are_the_same_regardless_of_product_variation_and_count($cart_items);
    $validationPassed = true;
    foreach ($products as $product_id => $product) {
        if ($product['count'] < 2) { // 최소 수량 검증 로직
            $message = "{$product['name']} - You must purchase a minimum of two products.";
            apmmust_manage_notices_optimized($message);
            $validationPassed = false;
        }
    }
    return $validationPassed;
}

// 주요 액션 및 필터 훅에 적용
add_filter('woocommerce_add_to_cart_validation', function ($passed, $product_id, $quantity) {
  apmmust_manage_notices_optimized(null, true); // 에러 메시지 초기화

    $cart_items = WC()->cart->get_cart();
    // 상품을 새로 담을 때의 상황을 고려하여, 현재 추가하는 상품도 포함시켜 검증 로직을 수행
    if (isset($cart_items[$product_id])) {
        $cart_items[$product_id]['quantity'] += $quantity;
    } else {
        $cart_items[] = array('product_id' => $product_id, 'quantity' => $quantity, 'data' => wc_get_product($product_id));
    }

    if (!apmmust_validate_product_quantities($cart_items)) {
        return false; // 검증 실패 시 false 반환
    }
    return $passed; // 기존의 검증 결과를 그대로 반환
}, 9999, 3);

// 상품 갯수 업데이트 시 에러 메시지 업데이트
add_action('woocommerce_after_cart_item_quantity_update', function ($cart_item_key, $quantity, $old_quantity, $cart) {
    if (!is_cart()) return; // 상품 상세페이지에서는 처리하지 않음
    apmmust_validate_product_quantities($cart->get_cart());
}, 9999999999, 4);

// Cart에서 error를 검증 및 표시
add_action('woocommerce_check_cart_items', function () {
    apmmust_validate_product_quantities(WC()->cart->get_cart());
});

// Cart item 제거 시 에러 메시지 업데이트
add_action('woocommerce_cart_item_removed', function ($cart_item_key, $cart) {
    apmmust_validate_product_quantities($cart->get_cart());
}, 10, 2);

// Checkout 페이지 변경 및 주문 검증 로직 추가
add_action('woocommerce_order_button_html', function ($button_html) {
    if (!apmmust_validate_product_quantities(WC()->cart->get_cart())) {
        return '<a href="' . esc_url(wc_get_cart_url()) . '" class="button alt">' . esc_html__('Go to Cart', 'woocommerce') . '</a>';
    }
    return $button_html;
}, 9999999999);

// Checkout 프로세스 전 검증
add_action('woocommerce_before_checkout_process', function () {
    if (!apmmust_validate_product_quantities(WC()->cart->get_cart())) {
        throw new Exception("You cannot process checkout. Please check your cart again.");
    }
});



//////////////////////////////////////////////////////////////////////////
$attribute_term_slugs = [
  'product_cat',
  'product_brand',
  'pa_colour',
  'pa_gender',
  'pa_made-in',
  'pa_season',
  'pa_size',
  'pa_composition',
  'product_tag',
];
foreach ($attribute_term_slugs as $slug) {
  add_action( 'init', function() use ($slug) {
    add_filter( "manage_edit-{$slug}_columns", function( $columns ) {
        $_columns = array(
          'cb' => $columns['cb'],
          'name' => $columns['name'],
          'korean_name' => 'K-Name',
        );
        unset($columns['cb']);
        unset($columns['name']);
        $new_columns = array_merge($_columns, $columns);
        return $new_columns;
    });
  });

  add_action( 'init', function() use ($slug) {
    add_filter( "manage_{$slug}_custom_column", function( $content, $column_name, $term_id ) {
        if ( 'korean_name' === $column_name ) {
            $korean_name = rwmb_meta( 'apmmust_vendor_korean_term_name', [ 'object_type' => 'term' ], $term_id );
            $content = empty( $korean_name ) ? '미지정' : $korean_name;
        }
        return $content;
    }, 10, 3 );
  });
}

// product category에 HS_Code추가하고,
// pdf invoice에 추가로 값넣기
add_filter('rwmb_meta_boxes', 'apmmust_hscode_field_in_product_category');
function apmmust_hscode_field_in_product_category($meta_boxes)
{
  $prefix = '';

  $meta_boxes[] = [
    'title' => __('HS_Code', 'apmmust'),
    'id' => 'hs_code',
    'taxonomies' => ['product_cat'],
    'fields' => [
      [
        'name' => __('HSCode', 'apmmust'),
        'id' => $prefix . 'hscode_text',
        'type' => 'text',
      ],
    ],
  ];

  return $meta_boxes;
}

add_action('wpo_wcpdf_after_item_meta', function ($document_type, $item, $document_order) {
  if (empty($item['product']))
    return;
  if (empty($item['product_id']))
    return;

  $product_id = $item['product_id'];
  $terms = get_the_terms($product_id, 'product_cat');
  $category_ids = array();

  if ($terms && !is_wp_error($terms)) {
    foreach ($terms as $term) {
      $category_ids[] = $term->term_id;
    }
  }
  if (count($category_ids) === 0) {
    return;
  }

  if (function_exists('rwmb_meta')) {
    $category_id = $category_ids[0];
    $value = rwmb_meta('hscode_text', ['object_type' => 'term'], $category_id);
    if ($value) {
      ob_start(); ?>
      <dl class="meta">
        <dt class="hscode">HSCode:</dt>
        <dd class="hscode" style="margin-left: -1px;">
          <?php echo $value; ?>
        </dd>
      </dl>
      <?php
      echo ob_get_clean();
    }
  }
}, 999, 3);
