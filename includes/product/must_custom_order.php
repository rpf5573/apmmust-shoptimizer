<?php
// 원바이투 23 AW 시즌 아이템 아프터아워스에게만 보이기
add_action('woocommerce_product_query', 'custom_product_visibility_for_specific_users');

function custom_product_visibility_for_specific_users($q) {
    $allowed_products = array(11117495, 11117498, 11117502, 11117505, 11117509, 11117512, 11117516, 11117520, 11117523, 11117527, 11117533, 11117539, 11117542, 11117546); // 특정 상품 ID 목록
    $allowed_users = array(1308, 31, 110); // 특정 사용자 ID 목록
    $current_user = get_current_user_id();

    if (!in_array($current_user, $allowed_users)) {
        $q->set('post__not_in', $allowed_products); // 허용된 사용자가 아니면, 이 상품들을 숨김
    }
}

add_filter('woocommerce_product_is_visible', 'custom_single_product_visibility_for_specific_users', 10, 2);

function custom_single_product_visibility_for_specific_users($visible, $product_id) {
    $allowed_products = array(11117495, 11117498, 11117502, 11117505, 11117509, 11117512, 11117516, 11117520, 11117523, 11117527, 11117533, 11117539, 11117542, 11117546); // 특정 상품 ID 목록
    $allowed_users = array(1308, 31, 110); // 특정 사용자 ID 목록
    $current_user = get_current_user_id();

    if (in_array($current_user, $allowed_users) && in_array($product_id, $allowed_products)) {
        return true; // 허용된 사용자와 상품이면 보임
    } elseif (in_array($product_id, $allowed_products)) {
        return false; // 허용된 상품이지만 사용자가 목록에 없으면 숨김
    }

    return $visible; // 그 외의 경우 기본 설정을 따름
}
// 원바이투 최소수량 10개
add_filter('woocommerce_quantity_input_min', 'custom_minimum_order_quantity_for_specific_products', 10, 2);
add_filter('woocommerce_quantity_input_args', 'custom_minimum_order_quantity_for_specific_products_args', 10, 2);

function custom_minimum_order_quantity_for_specific_products($min, $product) {
    $specific_product_ids = array(11117495, 11117498, 11117502, 11117505, 11117509, 11117512, 11117516, 11117520, 11117523, 11117527, 11117533, 11117539, 11117542, 11117546); // 최소 주문 수량을 설정하려는 특정 상품 ID 목록
    if (in_array($product->get_id(), $specific_product_ids)) {
        return 10; // 최소 주문 수량을 10으로 설정
    }
    return $min; // 그 외 상품은 기본 설정 사용
}

function custom_minimum_order_quantity_for_specific_products_args($args, $product) {
    $specific_product_ids = array(11117495, 11117498, 11117502, 11117505, 11117509, 11117512, 11117516, 11117520, 11117523, 11117527, 11117533, 11117539, 11117542, 11117546); // 최소 주문 수량을 설정하려는 특정 상품 ID 목록
    if (in_array($product->get_id(), $specific_product_ids)) {
        $args['min_value'] = 10; // 최소 주문 수량을 10으로 설정
    }
    return $args;
}

add_action('woocommerce_after_cart_item_quantity_update', 'prevent_decrease_cart_item_quantity', 20, 4);
function prevent_decrease_cart_item_quantity($cart_item_key, $quantity, $old_quantity, $cart) {
    $specific_product_ids = array(11117495, 11117498, 11117502, 11117505, 11117509, 11117512, 11117516, 11117520, 11117523, 11117527, 11117533, 11117539, 11117542, 11117546); // 최소 주문 수량을 유지하려는 특정 상품 ID 목록
    $minimum_quantity = 10; // 설정하려는 최소 수량

    $product_id = $cart->cart_contents[$cart_item_key]['product_id'];

    // 특정 상품의 수량이 최소값 미만으로 변경되려고 할 경우
    if (in_array($product_id, $specific_product_ids) && $quantity < $minimum_quantity) {
        $cart->cart_contents[$cart_item_key]['quantity'] = $minimum_quantity; // 수량을 최소값으로 재설정
        wc_add_notice(sprintf('The minimum quantity for specific products is %s. Your cart has been updated.', $minimum_quantity), 'error');
    }
}
