<?php
// apM MUST 대량 구매 할인 기능 2024.02.08
// 
// 유저 그룹에 따른 최소 구매수량 설정
function get_minimum_quantity_based_on_user_group() {
  $user_id = get_current_user_id();
  // 캐시 키 생성
  $cache_key = 'minimum_quantity_' . $user_id;

  // 캐시에서 최소 구매 수량을 시도하여 가져옴
  $minimum_quantity = get_transient($cache_key);
  
  // 캐시에 데이터가 있다면 반환
  if ($minimum_quantity !== false) {
      return $minimum_quantity;
  }

  // 캐시에 데이터가 없다면, 사용자 그룹을 기반으로 최소 구매 수량 계산
  $user_group = get_user_meta($user_id, 'user_group', true);

  if (in_array($user_group, ['b2b_vip3', 'b2b_vip5'])) {
      $minimum_quantity = 3;
  } elseif (in_array($user_group, ['b2b_asia', 'b2b_kz_bg', 'temporary_asia', 'temporary_global'])) {
      $minimum_quantity = 10;
  } else {
      $minimum_quantity = 5;
  }

  // 계산된 최소 구매 수량을 캐시에 저장 (24시간 동안 유효)
  set_transient($cache_key, $minimum_quantity, DAY_IN_SECONDS);

  return $minimum_quantity;
}

// Udate_user_meta 호출 시 캐시 무효화 처리add_action('updated_user_meta', 'invalidate_minimum_quantity_cache', 10, 4);
function invalidate_minimum_quantity_cache_on_user_group_update($meta_id, $user_id, $meta_key, $_meta_value) {
  // 사용자 그룹 메타 데이터가 업데이트되는 경우에만 캐시 무효화
  if ($meta_key === 'user_group') {
      $cache_key = 'minimum_quantity_' . $user_id;
      delete_transient($cache_key);
  }
}
add_action('updated_user_meta', 'invalidate_minimum_quantity_cache_on_user_group_update', 10, 4);


// 환율 플러그인 로직
function get_current_currency_rate() {
  $currentCurrency = get_current_currency(); // 현재 화폐 단위 가져오기
  $cache_key = 'current_currency_rate_' . $currentCurrency; // 화폐 단위별로 별도의 캐시 키 생성

  $cached_rate = get_transient($cache_key);
  if (false !== $cached_rate) {
      return $cached_rate;
  }

  $multiCurrencySettings = WOOMULTI_CURRENCY_Data::get_ins();
  $wmcCurrencies = $multiCurrencySettings->get_list_currencies();
  $currentCurrencyRate = floatval($wmcCurrencies[$currentCurrency]['rate']);

  set_transient($cache_key, $currentCurrencyRate, HOUR_IN_SECONDS); // 화폐 단위별 캐시 저장

  return $currentCurrencyRate;
}


function get_current_currency()
{
  $multiCurrencySettings = WOOMULTI_CURRENCY_Data::get_ins();
  return $multiCurrencySettings->get_current_currency();
}

// 상품 카테고리 기타할인금액 변경
function get_cached_category_ids($category_id)
{
  // 카테고리 ID를 기반으로 한 캐시 키 생성
  $cache_key = 'category_children_' . $category_id;
  $category_ids = get_transient($cache_key);

  if (false === $category_ids) {
    // 캐시된 데이터가 없으면, 하위 카테고리 ID를 조회
    $category_ids = get_term_children($category_id, 'product_cat');
    $category_ids[] = $category_id; // 기준 카테고리도 포함

    // 결과를 캐싱 (예: 1일 동안)
    set_transient($cache_key, $category_ids, DAY_IN_SECONDS);
  }

  return $category_ids;
}

function is_product_in_category_etc($product_id)
{
  $category_id = 1384; // 기준 카테고리 ID
  $category_ids = get_cached_category_ids($category_id);

  foreach ($category_ids as $cat_id) {
    if (has_term($cat_id, 'product_cat', $product_id)) {
      return true; // 상품이 지정된 카테고리 또는 그 하위 카테고리 중 하나에 속함
    }
  }
  return false; // 상품이 해당 카테고리에 속하지 않음
}

// 총 수량 계산 함수
function calculate_total_quantities_for_each_product()
{
  $quantities = array();
  foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
    $product_id = $cart_item['product_id']; // 상품 기본 ID
    if (!isset($quantities[$product_id])) {
      $quantities[$product_id] = 0;
    }
    $quantities[$product_id] += $cart_item['quantity'];
  }
  return $quantities;
}

// 대량 구매 디스카운트를 위한 기능 확장
add_filter('woocommerce_cart_item_name', 'custom_cart_item_name_with_bulk_discount_badge', 10, 3);
function custom_cart_item_name_with_bulk_discount_badge($name, $cart_item, $cart_item_key) {
    $required_quantity = get_minimum_quantity_based_on_user_group();
    $product_id = $cart_item['product_id'];

    // 환율 플러그인 로직을 사용하여 할인액 계산
    $currentCurrencyRate = get_current_currency_rate();
    $discount_per_product = is_product_in_category_etc($product_id) ? 200 : 1000; // 조건에 따른 할인 금액
    $discount_per_product_converted = $discount_per_product * $currentCurrencyRate;

    // 카트 내 모든 상품의 총 수량 계산
    $products_quantities = calculate_total_quantities_for_each_product();
    $product_base_id = $cart_item['product_id'];
    $total_quantity = $products_quantities[$product_base_id];

    if ($total_quantity >= $required_quantity) {
        $individual_discount = $discount_per_product_converted * $cart_item['quantity'];
        $formatted_discount = wc_price($individual_discount, array('currency' => get_current_currency()));

        // 할인 배지 추가
        $discount_badge = '<span class="discount-badge">Discounted</span>';
        // 할인 정보와 함께 배지 표시
        $discount_info = sprintf('<div class="discount-info" style="color: red;">Volume Discount - %s</div>', $formatted_discount);
        return $name . ' ' . $discount_badge . $discount_info;
    }

    return $name;
}


add_action('woocommerce_cart_calculate_fees', 'apply_bulk_discount_on_cart_total_extended', 10, 1);
function apply_bulk_discount_on_cart_total_extended($cart)
{
  if (is_admin() && !defined('DOING_AJAX'))
    return;

  $currentCurrencyRate = get_current_currency_rate();
  $required_quantity = get_minimum_quantity_based_on_user_group();
  $total_discount = 0; // 총 할인액 초기화

  // 카트 내 모든 상품의 총 수량 계산
  $products_quantities = calculate_total_quantities_for_each_product();

  foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
    $product_id = $cart_item['product_id'];
    $quantity = $cart_item['quantity'];

    // "socks" 카테고리에 속하는지 확인
    $is_socks = is_product_in_category_etc($product_id);
    $discount_per_product = $is_socks ? 200 : 1000; // "socks" 카테고리면 200원, 아니면 1000원 할인

    if (isset($products_quantities[$product_id]) && $products_quantities[$product_id] >= $required_quantity) {
      $total_discount += $discount_per_product * $currentCurrencyRate * $quantity;
    }
  }

  if ($total_discount > 0) {
    $cart->add_fee(__('Volume Discount', 'your-text-domain'), -$total_discount);
  }
}

// 상품 메타 데이터에 할인 정보 기록
add_action('woocommerce_checkout_update_order_meta', 'record_discounted_items_in_order_meta');

function record_discounted_items_in_order_meta($order_id) {
	$order = wc_get_order($order_id);
	$items = $order->get_items();
	$discounted_items = [];
	$required_quantity = get_minimum_quantity_based_on_user_group(); // 최소 구매 수량 가져오기

	foreach ($items as $item_id => $item) {
			$product_id = $item->get_product_id();
			$quantity = $item->get_quantity();

			// 할인 적용 조건 검사: 최소 구매 수량을 충족하는지 확인
			if ($quantity >= $required_quantity) {
					$product = $item->get_product();
					$product_name = $product->get_name(); // 상품 이름 가져오기

					// 할인 적용된 상품만 배열에 추가
					$discounted_items[] = sprintf("%s, %d", $product_name, $quantity);
			}
	}

	if (!empty($discounted_items)) {
			// 할인된 상품이 있는 경우, '|'로 구분하여 문자열 생성
			$discounted_items_str = implode(' | ', $discounted_items);
			// 주문 메타 데이터에 할인된 상품 정보 기록
			update_post_meta($order_id, 'volume_discounted_items', $discounted_items_str);
	}
}


// 상품 상세 페이지에 추가정보 표시
add_action('woocommerce_admin_order_data_after_order_details', 'display_discounted_items_in_order_details');
function display_discounted_items_in_order_details($order) {
	$discounted_items_str = $order->get_meta('volume_discounted_items');

	// 할인된 상품이 있는지 확인하고, 비어있으면 함수 실행을 중단
	if (empty($discounted_items_str)) {
			return; // 할인된 상품이 없으면 여기서 함수 실행을 중단
	}

	echo '<div class="order_data_column">';
	echo '<h4>Discounted Items <span class="tips" data-tip="This section shows discounted items and their quantities.">[?]</span></h4>';
	echo '<p>' . esc_html($discounted_items_str) . '</p>';
	echo '</div>';
}
